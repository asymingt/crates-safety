#include "contiki-conf.h"

extern void uart1_putc(char c);

unsigned int
dbg_send_bytes(const unsigned char *s, unsigned int len)
{
	unsigned int i=0;
	while(s && *s!=0) {
		if( i >= len) { break; }
		dbg_putchar(*s++); i++;
	}
	return i;
}
