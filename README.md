Overview
========

The CRATES-SAFETY project is related to the [CRATES](https://bitbucket.org/asymingt/crates-safety) project. Unlike CRATES, which provides a best-effort framework for controlling multiple robot platforms, CRATES-SAFETY provides mission-critical safety mechanisms for protecting people, property and platforms. In particular, it provides:

1. **Wireless tethering** - The base station transmits a heartbeat signal, which is received by all platforms. If a platform moves out of communication range of a base station it automatically enters LAND mode. If the LAND instruction cannot be carried out (FCS failure) then the power to the system is cut.

2. **Polygon zoning** - A zone is a polygonal area defined by a sequence of waypoints. SAFE and LAND zones are defined for the platform. The idea is to always fly in the SAFE zone. However, the platform moves out of the SAFE zone a LAND instruction is issued (as above). If the platform moves outside of both zones, then power to the platform is cut.

3. **Collision avoidance** - Each platform transmits a heartbeat signal, containing its position and velocity. Neighbouring platforms use this information to predict collisions. A linear trajectory is assumed with a lookup period of T seconds. If the two trajectories come within D meters of each other in this lookup period, evasive action is taken.

4. **Black box** - For traceability and diagnosis, as much data as possible is logged to a MicroSD card on board the platform. This includes all important syslog events, platform low-level data, RAW and RTCM position data and packets received on the network. 

5. **Position aiding** - RAW GNSS measurements are forwarded to a base station, which calculates and returns RTCM corrections, allowing local DGPS / RTK to take place to improve the platform's position estimate.

Architecture
============

CRATES-SAFETY is built upon Contiki OS, and can be described by the following block diagram:

![Quadrotor Safety System](https://bytebucket.org/asymingt/crates-safety/raw/a3bd48a389da0ebfc1891dae21d7ad8d48874903/targets/safety/safety.png?token=291b0f1c477e58fc36865d5cfa2b9d0ed3f9a5ed "Block diagram for the safety system")