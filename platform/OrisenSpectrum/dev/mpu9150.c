/*
 * mpu9150.c
 *
 *  Created on: 16 Sep 2013
 *      Author: mcphillips
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "i2c.h"
#include "include/mpu9150.h"
#include "lib/sensors.h"
#include <mc1322x.h>

void mpu9150_mag_who_am_i(uint8_t *buffer) {

	uint8_t request_MSB = MPU9150_MAG_WIA ;

	i2c_transmitinit( MPU9150_MAG_I2C_ADDR, 1, &request_MSB ) ;
	while(!i2c_transferred()) /* Wait for transfer */ ;

	i2c_receiveinit( MPU9150_MAG_I2C_ADDR, 2, buffer ) ;
	while(!i2c_transferred()) /* Wait for transfer */ ;
};


void mpu9150_who_am_i(uint8_t *buffer) {

	uint8_t request_MSB = MPU9150_WHO_AM_I ;

	i2c_transmitinit( MPU9150_I2C_ADDR, 1, &request_MSB ) ;
	while(!i2c_transferred()) /* Wait for transfer */ ;

	i2c_receiveinit( MPU9150_I2C_ADDR, 2, buffer ) ;
	while(!i2c_transferred()) /* Wait for transfer */ ;
}

void mpu9150_mag_start(void) {

//	uint8_t receive = 0 ;
	uint8_t set[] = {0,0} ;

	set[0] = MPU9150_MAG_CNTL ;
	set[1] = 0x01;		// Single Measurement Mode

	i2c_transmitinit( MPU9150_MAG_I2C_ADDR, 2, set ) ;
	while(!i2c_transferred()) /* Wait for transfer */ ;

}

void mpu9150_mag_get_data(uint8_t *buffer) {

	uint8_t request_MSB = MPU9150_MAG_XOUT_L ;

	i2c_transmitinit( MPU9150_MAG_I2C_ADDR, 1, &request_MSB ) ;
	while(!i2c_transferred()) /* Wait for transfer */ ;

	i2c_receiveinit( MPU9150_MAG_I2C_ADDR, 6, buffer ) ;
	while(!i2c_transferred()) /* Wait for transfer */ ;
}

void mpu9150_mag_decode(struct mpu9150_mag_data *const ba , uint8_t *buffer) {

	ba->mag_x = ((buffer[1]<<8) + buffer[0]);

	ba->mag_y = ((buffer[3]<<8) + buffer[2]);

	ba->mag_z = ((buffer[5]<<8) + buffer[4]);

}

void mpu9150_get_int_status(uint8_t *buffer) {
	
	uint8_t request_MSB = MPU9150_INT_STATUS ;

	i2c_transmitinit( MPU9150_I2C_ADDR, 1, &request_MSB ) ;
	while(!i2c_transferred()) /* Wait for transfer */ ;

	i2c_receiveinit( MPU9150_I2C_ADDR, 2, buffer ) ;
	while(!i2c_transferred()) /* Wait for transfer */ ;
}

void mpu9150_decode_int(struct mpu9150_int_data *const ba, uint8_t *buffer) {

	ba->fifo_oflow = ((buffer[0] & 0b00010000) >> 4);
	ba->mst = ((buffer[0] & 0b00001000) >> 3);
	ba->data_rdy = (buffer[0] & 0b00000001);
}

void mpu9150_get_data(uint8_t *buffer) {

	uint8_t request_MSB = MPU9150_ACCEL_XOUT_H ;

	i2c_transmitinit( MPU9150_I2C_ADDR, 1, &request_MSB ) ;
	while(!i2c_transferred()) /* Wait for transfer */ ;

	i2c_receiveinit( MPU9150_I2C_ADDR, 14, buffer ) ;
	while(!i2c_transferred()) /* Wait for transfer */ ;
}

void mpu9150_get_fifo(uint8_t *buffer, uint16_t read_bytes) {

	uint8_t request_MSB = MPU9150_FIFO_R_W ;

	i2c_transmitinit( MPU9150_I2C_ADDR, 1, &request_MSB ) ;
	while(!i2c_transferred()) /* Wait for transfer */ ;
	
	i2c_receiveinit( MPU9150_I2C_ADDR, read_bytes, buffer ) ;
	while(!i2c_transferred()) ; /* Wait for transfer */ 
}


void mpu9150_get_fifo_count(uint16_t *buffer) {

	uint8_t receive[] = {0,0} ;
	uint8_t request_MSB = MPU9150_FIFO_COUNT_H ;

	i2c_transmitinit( MPU9150_I2C_ADDR, 1, &request_MSB ) ;
	while(!i2c_transferred()) /* Wait for transfer */ ;

	i2c_receiveinit( MPU9150_I2C_ADDR, 2, receive ) ;
	while(!i2c_transferred()) /* Wait for transfer */ ;

	*buffer = (((receive[0] & 0x07) <<8) + receive[1]);
}

void mpu9150_decode(struct mpu9150_data *const ba , uint8_t *buffer) {

	ba->acc_x = ((buffer[0]<<8) + buffer[1]);

	ba->acc_y = ((buffer[2]<<8) + buffer[3]);

	ba->acc_z = ((buffer[4]<<8) + buffer[5]);

	ba->temp = ((buffer[6]<<8) + buffer[7]);

	ba->gyro_x = ((buffer[8]<<8) + buffer[9]);

	ba->gyro_y = ((buffer[10]<<8) + buffer[11]);

	ba->gyro_z = ((buffer[12]<<8) + buffer[13]);
}

void mpu9150_start(void) {

//	uint8_t receive = 0 ;
	/*uint8_t set[] = {0,0} ;

	set[0] = MPU9150_PWR_MGMT_1 ;
	set[1] = 0x40;		// Just switching it on - clear sleep bit

	i2c_transmitinit( MPU9150_I2C_ADDR, 2, set ) ;
	while(!i2c_transferred()) // Wait for transfer
	*/

}

void mpu9150_user_ctrl(void) {

//	uint8_t receive = 0 ;
	uint8_t set[] = {0,0} ;

	set[0] = MPU9150_USER_CTRL ;
	set[1] = 0x44;		// Enable FIFO and reset FIFO buffer

	i2c_transmitinit( MPU9150_I2C_ADDR, 2, set ) ;
	while(!i2c_transferred()) /* Wait for transfer */ ;

}

void mpu9150_init(void) {

//	uint8_t receive = 0 ;
	uint8_t set[] = {0,0} ;

	set[0] = MPU9150_PWR_MGMT_1 ;
	set[1] = 0x01;		// You must do this before doing anything else !!
	i2c_transmitinit( MPU9150_I2C_ADDR, 2, set ) ;
	while(!i2c_transferred()) /* Wait for transfer */ ;

	set[0] = MPU9150_CONFIG ;
	set[1] = (MPU9150_DLPF_CFG_10HZ + (MPU9150_EXT_SYNC_SET_IN_DISABLE<<3));		// Setting 10Hz low pass filter
	i2c_transmitinit( MPU9150_I2C_ADDR, 2, set ) ;
	while(!i2c_transferred()) /* Wait for transfer */ ;

	set[0] = MPU9150_SMPLRT_DIV ;
	set[1] = MPU9150_REQ_SMPLRT_DIV;		// 19 sets 50Hz sample rate (Sample Rate = Gyroscope Output Rate / (1 + SMPLRT_DIV)
	i2c_transmitinit( MPU9150_I2C_ADDR, 2, set ) ;
	while(!i2c_transferred()) /* Wait for transfer */ ;

	set[0] = MPU9150_INT_PIN_CFG ;
	set[1] = 0xDA;		// Data Ready interrupt (0xDA = 0xFA but pulse rather than held)
	i2c_transmitinit( MPU9150_I2C_ADDR, 2, set ) ;
	while(!i2c_transferred()) /* Wait for transfer */ ;

	// Read interrupt status to clear any existing interrupt
	uint8_t request_MSB = MPU9150_INT_STATUS ;
	uint8_t buffer;
	i2c_transmitinit( MPU9150_I2C_ADDR, 1, &request_MSB ) ;
	while(!i2c_transferred()) /* Wait for transfer */ ;
	i2c_receiveinit( MPU9150_I2C_ADDR, 2, &buffer ) ;
	while(!i2c_transferred()) /* Wait for transfer */ ;

	set[0] = MPU9150_INT_ENABLE ;
	set[1] = 0x01;		// Data Ready interrupt,
	i2c_transmitinit( MPU9150_I2C_ADDR, 2, set ) ;
	while(!i2c_transferred()) /* Wait for transfer */ ;

	set[0] = MPU9150_FIFO_EN ;
	set[1] = 0xF8;		// Data Ready interrupt,
	i2c_transmitinit( MPU9150_I2C_ADDR, 2, set ) ;
	while(!i2c_transferred()) /* Wait for transfer */ ;

	set[0] = MPU9150_USER_CTRL ;
	set[1] = 0x44;		// Enable FIFO and reset FIFO buffer, also enable passthrough

	i2c_transmitinit( MPU9150_I2C_ADDR, 2, set ) ;
	while(!i2c_transferred()) /* Wait for transfer */ ;


}

/* Add various methods I think are related to Contiki sensor set-up */
/**
 * Function to perform a logical shift right.
 */
uint8_t lsr(uint8_t x, uint8_t n) {
  return (uint8_t)((unsigned int)x >> n);
}

/**
 * Function to perform a logical shift left.
 */
uint8_t lsl(uint8_t x, uint8_t n) {
  return (uint8_t)((unsigned int)x << n);
}

static int
value(int type)
{
    // TODO
    return -1;
}

static int
status(int type)
{
    // TODO
    return 1;
}

static int
configure(int type, int c)
{
	mpu9150_init();
	mpu9150_start();
  return 1;
}

char *int2bin(uint8_t a, char *buffer, int buf_size) {
    buffer += (buf_size - 1);

    int i = 7;
    for (i ; i >= 0; i--) {
        *buffer-- = (a & 1) + '0';

        a >>= 1;
    }

    return buffer;
}

/*******************************************************************************
 * Function  to write a buffer of uint8s to the sd card.                       *
 *******************************************************************************/
void mpu9150_write_to_sd(uint8_t *buffer) {
    
}

// Instantiate the sensor
//const struct sensors_sensor mpu9150_sensor = {"MPU9150-Sensor", value, configure, status};
SENSORS_SENSOR(mpu9150_sensor, "MPU9150-Sensor", value, configure, status);
