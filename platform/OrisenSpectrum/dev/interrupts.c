
void kbi7_init(void) {
	kbi_edge(7);
	enable_ext_wu(7);
	kbi_pol_neg(7);
	GPIO->PAD_DIR.KBI_7 = 0;
	GPIO->PAD_PU_SEL.KBI_7 = 1;
	GPIO->PAD_PU_EN.KBI_7 = 1;
	GPIO->PAD_HYST_EN.KBI_7 = 1;
	clear_kbi_evnt(7);
}

void kbi7_isr(void) {
	if (GPIO->DATA.INT_MPU == 0) m++;
	kbi7_flag = 1;
	printf("In kbi7_isr\n)");
//	PRINTF("Gone into KBI 7\n") ;
//	r = r + GPIO->DATA.INT_RTC;
//	if (!GPIO->DATA.GPS_TIMEPULSE) g++;
//	LED4_ON;
	clear_kbi_evnt(7);
}

void sd_card_switch_init(void) {
	GPIO->FUNC_SEL.SD_CARD_SWITCH = SD_CARD_INIT_FUNC_SEL;
	GPIO->PAD_DIR.SD_CARD_SWITCH = 0;
	GPIO->PAD_PU_SEL.SD_CARD_SWITCH = 1;
	GPIO->PAD_PU_EN.SD_CARD_SWITCH = 1;
	GPIO->PAD_HYST_EN.SD_CARD_SWITCH = 1;
#if SD_POWER_CTL
	GPIO->PAD_DIR.SD_CARD_PWR = 1;
	SD_CARD_OFF;
#endif
}