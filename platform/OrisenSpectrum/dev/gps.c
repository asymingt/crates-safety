/**
 * \defgroup GPS-driver LEA-6T GPS driver for the Orisen Spectrum Basic
 *
 * This is a driver for the LEA-6T GPS chip. It should also work with the
 * LEA-4T, which is pin compatible with the 6T since we don't use anything
 * special about the 6T. We just gather appropriate NMEA and UBX messages
 *
 * @{
 */

/**
 * \file
 *         This is a driver for the GPS chips on the Orisen Spectrum Basic
 * \author
 *         Stephen Hailes
 * \author
 *         Graeme McPhillips
 * 
 */

#include <stdio.h>
#include <string.h>
#include "mc1322x.h"
#include "clock.h"
#include "include/gps.h"

#define DEBUG 1

#if DEBUG
#include <stdio.h>
#define PRINTF(...) printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif

// The buffer in which we store one NMEA line for parsing.
//
char nmea_msg_buf[NMEA_MAXLEN];
int nmea_msg_len = 0;

// This struct stores UBX GPS data temporarily when parsing
//
struct GPSUBX gps_ubx;

// This flag will be set to TRUE once a complete packet has been received.
// Used by gps_nmea_parse() below
volatile short gps_msg_received = 0;

void reset_gps_ubx() {
    gps_ubx.msg_available = 0;
    //gps_ubx.msg_buf[GPS_UBX_MAX_PAYLOAD] __attribute__ ((aligned));
    gps_ubx.msg_id = 0;
    gps_ubx.msg_class = 0;

    gps_ubx.status = UNINIT; //???????
    gps_ubx.len = 0;
    gps_ubx.msg_idx = 0;
    gps_ubx.ck_a = 0, gps_ubx.ck_b = 0;
    gps_ubx.send_ck_a = -1, gps_ubx.send_ck_b = -1;
    gps_ubx.error_cnt = 0;
    gps_ubx.error_last = GPS_UBX_ERR_NONE;

    gps_ubx.status_flags = 0;
    gps_ubx.sol_flags = 0;
    gps_ubx.have_velned = 0;
}

/*---------------------------------------------------------------------------*/

/**
 * \brief			Calculate and insert the packet checksum
 *
 *						Calculate the packet checksum from all bytes except the first
 *						two bytes of header and the checksum bytes themselves. Insert
 *						it into the last two bytes. The checksum uses a Fletcher
 *						Algorithm
 */

void insert_checksum(unsigned char *packet, int size) {
    unsigned char *payload = &packet[2];
    unsigned char *checksum = &packet[size - 2];
    uint8_t a, b;
    a = b = 0x00;

    size = size - 4; // Disregard the sync and checksum bytes

    while (size-- > 0) {
        a += *(payload++);
        b += a;
    }

    checksum[0] = a;
    checksum[1] = b;
}


/*---------------------------------------------------------------------------*/

/**
 * \brief			Write a packet to the ublox chip
 *
 *						Write a packet to the ubloc chip
 */
void gps_write(char *buf, int size) {
    int i;

    printf("\n OUT>");
    for (i = 0; i < size; i++)
        printf("%02X ", buf[i]);
    printf("\n");

    for (i = 0; i < size; i++)
        uart2_putc(buf[i]);
}


/*---------------------------------------------------------------------------*/

/**
 * \brief			Write a packet to the ublox chip
 *
 *						Write a packet to the ubloc chip
 */
void gps_getAck() {
    uint8_t packet[10];
    int i = -1;
    int j = 0;

    clock_time_t timeoutTime = clock_time() + CLOCK_CONF_SECOND * 0.5;

    while (clock_time() < timeoutTime) {
        if (!uart2_can_get()) {
            clock_delay_msec(2);
            continue;
        }

        while (uart2_can_get()) {
            uint8_t c = uart2_getc();

            if (c == 0xB5) {
                i = 0;
            }

            if (i >= 0 & i < 10) {
                packet[i] = c;
                i++;
            }

            if (i == 10) {
                printf("ACK ");
                for (j = 0; j < 8; j++)
                    printf("%02X ", packet[j]);
                printf("\n");
                return;
            }
        }
    }

    printf("ACK Timed out\n");


}


/*---------------------------------------------------------------------------*/

/**
 * \brief			Configure messages
 *
 *						Choose which messages are generated. We set the rate that the
 *						ublox should send a message relative to the rate that the event
 *						occurs - e.g. if a navigation message has rate 2, it is sent
 *						once every second navigation solution.
 */
void set_CFG_MSG(uint8_t msgClass, uint8_t msgID, uint8_t rate) {
    unsigned char packet[8 + UBX_PACKET_FRAMING_SIZE];
    unsigned char *payload = &packet[6];

    // Form the header
    //
    packet[0] = 0xB5; // Sync 1
    packet[1] = 0x62; // Sync 2
    packet[2] = 0x06; // Class
    packet[3] = 0x01; // ID
    packet[4] = 0x08; // Length low byte 	(8 decimal)
    packet[5] = 0x00; // Length high byte

    // Now build the payload
    //
    payload[ 0] = msgClass; // [1] message class
    payload[ 1] = msgID; // [1] message ID
    payload[ 2] = 0; // [1] rate for I2C
    payload[ 3] = rate; // [1] rate for UART1
    payload[ 4] = 0; // [1] rate for UART2
    payload[ 5] = 0; // [1] rate for USB
    payload[ 6] = 0; // [1] rate for SPI
    payload[ 7] = 0; // [1] reserved

    insert_checksum(packet, sizeof (packet)); // Form the checksum for this packet
    gps_write(packet, sizeof (packet)); // And send it
    gps_getAck();
}

/*---------------------------------------------------------------------------*/

/**
 * \brief			Configure the port on the LEA-6T
 *
 *						Set baud rate of the port on the LEA-6T
 */
void set_CFG_PRT(uint32_t baudrate) {
    unsigned char packet[20 + UBX_PACKET_FRAMING_SIZE];
    unsigned char *payload = &packet[6];

    // Form the header
    //
    packet[0] = 0xB5; // Sync 1
    packet[1] = 0x62; // Sync 2
    packet[2] = 0x06; // Class
    packet[3] = 0x00; // ID
    packet[4] = 0x14; // Length low byte 	(20 decimal)
    packet[5] = 0x00; // Length high byte

    // Now build the payload
    //
    payload[ 0] = 0x01; // [1] Set Port to 1 = UART 1
    payload[ 1] = 0x00; // [1] Reserved
    payload[ 2] = 0x01; // [2] txReady enabled, active high, pin 0, no threshold		?????
    payload[ 3] = 0x00; // 
    payload[ 4] = 0xD0; // [4] mode. 8-1-N 
    payload[ 5] = 0x08; //
    payload[ 6] = 0x00; //
    payload[ 7] = 0x00; //
    payload[ 8] = ((baudrate & 0xFF)); // [4] Baud rate. 
    payload[ 9] = ((baudrate >> 8) & 0xFF); //
    payload[10] = ((baudrate >> 16) & 0xFF); //
    payload[11] = ((baudrate >> 24) & 0xFF); //
    payload[12] = 0x03; // [2] Active input protocols:  NMEA = 2; UBX = 1
    payload[13] = 0x00; // 
    payload[14] = 0x03; // [2] Active output protocols: NMEA = 2; UBX = 1
    payload[15] = 0x00; // 
    payload[16] = 0x00; // [2] Always zero
    payload[17] = 0x00; //
    payload[18] = 0x00; // [2] Always zero
    payload[19] = 0x00; //

    insert_checksum(packet, sizeof (packet)); // Form the checksum for this packet
    gps_write(packet, sizeof (packet)); // And send it
    gps_getAck();
}

/*---------------------------------------------------------------------------*/

/**
 * \brief			Set the rate of measurement and time alignment
 *
 *						Set the rate of measurement and time alignment (to UTC)
 */
static void set_CFG_RATE(uint16_t mRate) {
    unsigned char packet[6 + UBX_PACKET_FRAMING_SIZE];
    unsigned char *payload = &packet[6];

    // Form the header
    //
    packet[0] = 0xB5; // Sync 1
    packet[1] = 0x62; // Sync 2
    packet[2] = 0x06; // Class
    packet[3] = 0x08; // ID
    packet[4] = 0x06; // Length low byte 	(6 decimal)
    packet[5] = 0x00; // Length high byte

    // Now build the payload
    //
    payload[ 0] = (mRate & 0xFF); // [2] Measurement rate in ms (250)
    payload[ 1] = (mRate >> 8) & 0xFF; //
    payload[ 2] = 0x01; // [2] Navigation rate. Must be 1
    payload[ 3] = 0x00; //
    payload[ 4] = 0x00; // [2] Alignment to reference time: 0 = UTC, 1 = GPS
    payload[ 5] = 0x00; //

    insert_checksum(packet, sizeof (packet)); // Form the checksum for this packet
    gps_write(packet, sizeof (packet)); // And send it
    gps_getAck();
}

/*---------------------------------------------------------------------------*/

/**
 * \brief			Get the time pulse configuration
 *
 *						Get the time pulse configuration
 */
static void get_CFG_TP() {
    uint8_t *tmpBuffer;
    uint16_t len;
    //CORRECT WAY ROUND??????????????????????? ALSO, CALL TO RESET FIRST???
    reset_gps_ubx();
    gps_ubx.msg_id = 0x31;
    gps_ubx.msg_class = 0x06;
    get_ubx(gps_ubx);
    printf("HERE!!!!!!\n");
    int i;
    for (i = 0; i < len; i++) {
        printf("%i: 0x%02x \n", i, len, tmpBuffer[i]);
    }
    printf("Setting pulse to 1 sec period, 0.5 sec duration.\n");
    // Pulse already at 1s; 
    gps_ubx.msg_buf[16] = 0x20;
    gps_ubx.msg_buf[17] = 0xa1;
    gps_ubx.msg_buf[18] = 0x07;
    gps_ubx.msg_buf[19] = 0x00;
    // Create full message packet
    unsigned char packet[gps_ubx.len + UBX_PACKET_FRAMING_SIZE];
    packet[0] = 0xB5;
    packet[1] = 0x62;
    packet[2] = gps_ubx.msg_class;
    packet[3] = gps_ubx.msg_id;
    packet[4] = 32;
    packet[5] = 0;
    // Copy payload
    for (i = 0; i < gps_ubx.len; i++){
        packet[i+6] = gps_ubx.msg_buf[i];
    }
    insert_checksum(packet, sizeof (packet)); // Form the checksum for this packet
    gps_write(packet, sizeof (packet)); // And send it
    gps_getAck();
    // Check it took
    reset_gps_ubx();
    gps_ubx.msg_id = 0x31;
    gps_ubx.msg_class = 0x06;
    clock_delay_msec(2000);
    get_ubx(gps_ubx);
    printf("HERE AGAIN!!!!!!\n");
    for (i = 0; i < len; i++) {
        printf("%i: 0x%02x \n", i, len, tmpBuffer[i]);
    }
}

int get_CFG_TP5(struct GPSUBX in) {
    reset_gps_ubx();
    in.msg_class = 0x06;
    in.msg_id = 0x31;
    return get_ubx(in);
}

int get_ubx(struct GPSUBX in) {//uint8_t classid, uint8_t msgid, uint8_t *buffer, uint16_t *len) {
    // in.class_id and in.msg_id must contain the required class and message IDs for message
    // This method will discard any UBX packets that do not match the required class/msg ID

    // Ask for the right message type
    unsigned char packet[0 + UBX_PACKET_FRAMING_SIZE];
    // Form the header
    packet[0] = 0xB5; // Sync 1
    packet[1] = 0x62; // Sync 2
    packet[2] = in.msg_class; // Class
    packet[3] = in.msg_id; // ID
    packet[4] = 0x00; // Length low byte 	(0 decimal)
    packet[5] = 0x00; // Length high byte

    // No payload
    insert_checksum(packet, sizeof (packet)); // Form the checksum for this packet
    gps_write(packet, sizeof (packet)); // And send it

    char c1 = 0, c2 = 0;
    // Get packet from UART2:
    clock_time_t timeoutTime = clock_time() + CLOCK_CONF_SECOND * 2;

    while (clock_time() < timeoutTime) {
        if (!uart2_can_get()) {
            clock_delay_msec(2);
            continue;
        }
        while (uart2_can_get()) {
            c1 = c2;
            c2 = uart2_getc();
            if (c1 == SYNC_BYTE1 && c2 == SYNC_BYTE2) {
                in.status = GOT_SYNC2;
                // Now the sync bytes have been found, we want to check if the classID and msgID are what we want:
                c1 = uart2_getc();
                c2 = uart2_getc();
                printf("UBX packet found (classID: 0x%x; msgID: 0x%x);\n", c1, c2);
                if (c1 == in.msg_class && c2 == in.msg_id) {
                    in.status = GOT_ID;
                    printf("Required MSG found\n");
                    //Now we found the correct message identifiers, let's copy the payload into the buffer:
                    c1 = uart2_getc();
                    c2 = uart2_getc();
                    in.len = ((c2 << 8) | c1);
                    in.status = GOT_LEN2;
                    printf("len:%i", in.len);
                    if (in.len > GPS_UBX_MAX_PAYLOAD) {
                        in.error_cnt++;
                        in.error_last = GPS_UBX_ERR_MSG_TOO_LONG;
                        return -1; // Payload too big for receive buffer
                    }
                    int i;
                    for (i = 0; i < in.len; i++) {
                        in.msg_buf[i] = uart2_getc();
                    }
                    in.status = GOT_PAYLOAD;
                    c1 = uart2_getc(); //cA
                    c2 = uart2_getc(); //cB
                    in.status = GOT_CHECKSUM2;
                    // Check the checksum
                    if (verify_checksum(c1, c2, in.len, in.msg_buf)) {
                        printf("CKSUM Good!\n");
                        for (i = 0; i < in.len; i++) {
                            printf("%i: 0x%02x \n", i, in.msg_buf[i]);
                        }
                        printf("\n");
                        return 0;
                    } else {
                        in.error_cnt++;
                        in.error_last = GPS_UBX_ERR_CHECKSUM;
                        return -1;
                    }
                }
            }
        }
    }
    // Time out scanning uart2
    printf("UART EMPTY: Message not found\n");
    in.error_cnt++;
    in.error_last = GPS_UBX_ERR_REQ_MSG_NOT_FOUND;
    return -1;
}

int verify_checksum(uint8_t ca, uint8_t cb, uint16_t len, uint8_t *payload) {
    char CK_A = 0, CK_B = 0;
    int i;
    for (i = 0; i < len; i++) {
        CK_A = CK_A + payload[i];
        CK_B = CK_B + CK_A;
    }
    if (ca == CK_A && cb == CK_B) {
        return 1;
    } else {
        return -1;
    }
}


/*---------------------------------------------------------------------------*/

/**
 * \brief			Do a full reset of the chip
 *
 *						Do a full reset of the chip 
 */
static void set_CFG_RST(void) {
    unsigned char packet[4 + UBX_PACKET_FRAMING_SIZE];
    unsigned char *payload = &packet[6];

    // Form the header
    //
    packet[0] = 0xB5; // Sync 1
    packet[1] = 0x62; // Sync 2
    packet[2] = 0x06; // Class
    packet[3] = 0x04; // ID
    packet[4] = 0x04; // Length low byte 	(4 decimal)
    packet[5] = 0x00; // Length high byte

    // Now build the payload
    //
    payload[ 0] = 0x00; // [2] State to clear. 0 = hotstart, 1 = warmstart, 0xFFFF = coldstart
    payload[ 1] = 0x00; //
    payload[ 2] = 0x00; // [1] reset mode 0 = h/w reset
    payload[ 3] = 0x00; // [1] Reserved

    insert_checksum(packet, sizeof (packet)); // Form the checksum for this packet
    gps_write(packet, sizeof (packet)); // And send it
}


/*---------------------------------------------------------------------------*/

/**
 * \brief			Initialise the UBX processing engine
 *
 *						Initialise the state variables in the UBX processing engine
 */
void gps_ubx_init(void) {
    gps_ubx.status = UNINIT;
    gps_ubx.msg_available = 0;
    gps_ubx.error_cnt = 0;
    gps_ubx.error_last = GPS_UBX_ERR_NONE;
    gps_ubx.have_velned = 0;
}



/*---------------------------------------------------------------------------*/

/**
 * \brief			Initialise the GPS module
 *
 *						Initialise the GPS module to run at 115200 baud on the
 *						serial port
 */
void initialise_GPS(void) {

    uart2_init(63, MOD, SAMP); // 9600 baud - (63, MOD, SAMP) means 9600

    //set_CFG_RST();							// Execute a hardware reset
    //clock_delay_msec(2000);
    //printf("Reset\n");

    gps_ubx_init(); // Initialise UBX data structure
    set_CFG_PRT(9600); // Baudrate & port config - set to 115200

    // Now set UART2 to 115200 baud as we did the uBlox chip
    //
    //UART2->CON &= 0xFFFFFFFC;		// Disable RxE and TxE before setting new baud rate
    //UART2->BR = ( INC << 16 ) | MOD;
    //if (SAMP == UCON_SAMP_16X) 
    //	set_bit(*UART2_UCON, UCON_SAMP);
    //UART2->CON |= 0x00000003;		// Re-enable TX and RX

    set_CFG_RATE(1000); // number of milliseconds between samples (1000 = 1 sample/sec)
    get_CFG_TP(); // Set the timepulse
    // Third argument is multiple of rate set with set_CFG_RATE()
    set_CFG_MSG(0xF0, 0x00, 0); // Disable NMEA GGA msgs
    set_CFG_MSG(0xF0, 0x01, 1); // Enable NMEA GLL msgs
    set_CFG_MSG(0xF0, 0x02, 0); // Disable NMEA GSA msgs
    set_CFG_MSG(0xF0, 0x03, 0); // Disable NMEA GSV msgs
    set_CFG_MSG(0xF0, 0x04, 0); // Disable NMEA RMC msgs
    set_CFG_MSG(0xF0, 0x05, 0); // Disable NMEA VTG msgs
    set_CFG_MSG(0xF0, 0x06, 0); // Disable NMEA GRS msgs
    set_CFG_MSG(0xF0, 0x07, 0); // Disable NMEA GST msgs
    set_CFG_MSG(0xF0, 0x08, 0); // Disable NMEA ZDA msgs
    set_CFG_MSG(0xF0, 0x09, 0); // Disable NMEA GBS msgs
    set_CFG_MSG(0xF0, 0x0A, 0); // Disable NMEA DTM msgs
    set_CFG_MSG(0xF0, 0x0E, 0); // Disable NMEA THS msgs
    set_CFG_MSG(0xF0, 0x40, 0); // Disable NMEA GPQ msgs
    set_CFG_MSG(0xF0, 0x41, 0); // Disable NMEA TXT msgs

    set_CFG_MSG(0x01, 0x02, 0); // Disable NAV-POSLLH  // 28 + 8 = 36 bytes
    set_CFG_MSG(0x01, 0x03, 0); // Disable NAV-STATUS  // 16 + 8 = 24 bytes
    set_CFG_MSG(0x01, 0x04, 0); // Disable NAV-DOP     // 18 + 8 = 26 bytes
    set_CFG_MSG(0x01, 0x06, 0); // Disable NAV-SOL     // 52 + 8 = 60 bytes
    set_CFG_MSG(0x01, 0x12, 0); // Disable NAV-VELNED  // 36 + 8 = 44 bytes
    set_CFG_MSG(0x01, 0x30, 0); // Disable NAV-SVINFO  // (8 + 12 * x) + 8 = 112 bytes (@8)
    set_CFG_MSG(0x02, 0x11, 0); // Disable RXM-SFRB    // 42 + 8 = 50 bytes

    set_CFG_MSG(0x01, 0x21, 0); // NAV-TIMEUTC // 20 + 8 = 28 bytes
    set_CFG_MSG(0x02, 0x10, 0); // RXM-RAW     // (8 + 24 * x) + 8 = 208 bytes (@8)
    set_CFG_MSG(0x0B, 0x31, 0); // AID-EPH		 // (104 + 8) = 112 bytes, or (8 + 8) = 16 bytes

    /*
    // Save the current configuration (0 BBR, 1 FLASH, 2 i2c-EEPROM)
            uint8_t cfg_cfg[] = {0xB5, 0x62, 0x06, 0x09, 0x0D, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x07, 0x21, 0xAF};
     */
}


