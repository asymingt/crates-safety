#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "i2c.h"
#include "contiki.h"
#include "dev/leds.h"
#include "SteveTest.h"

/*---------------------------------------------------------------------------*/
PROCESS(SteveTest_process, "SteveTest");
AUTOSTART_PROCESSES(&SteveTest_process);
/*---------------------------------------------------------------------------*/

PROCESS_THREAD(SteveTest_process, ev, data)
{ uint8_t buf = 0;
  static struct etimer et;

  printf("Initialising power management...\n");
  // Chip starts in sleep mode and we need to turn it on: (REG:107)
  uint8_t cmd[] = {MPU9150_PWR_MGMT_1, 0x00} ;
  i2c_transmitinit( MPU9150_I2C_ADDR, 2, cmd ) ;
  while(!i2c_transferred()) /* Wait for transfer */ ;
  printf("Power management initialised!\n");

  buf = MPU9150_WHO_AM_I;
	i2c_transmitinit( MPU9150_I2C_ADDR, 1, &buf ) ;
	while(!i2c_transferred()) /* Wait for transfer */ ;
	i2c_receiveinit( MPU9150_I2C_ADDR, 1, &buf );
	while(!i2c_transferred()); /* Wait for transfer */	
}
