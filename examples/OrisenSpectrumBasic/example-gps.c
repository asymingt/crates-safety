#include <stdio.h>
#include "mc1322x.h"
#include "contiki.h"
#include "dev/leds.h"
#include "include/GPS.h"
#include "cfs/cfs.h"
#include "cfs/cfs-coffee.h"


PROCESS(gps_process, "GPS process");
AUTOSTART_PROCESSES(&gps_process);

#define FLASH_LED(l) {leds_on(l); clock_delay_msec(50); leds_off(l); clock_delay_msec(50);}

PROCESS_THREAD(gps_process, ev, data)
{
  static struct etimer et;
	static int fd;

  PROCESS_BEGIN();

	uSDcard_init();

  // Obtain a file descriptor for the file, capable of handling both 
  // reads and writes.
  //
  #define FILENAME "test001.ubx"
  fd = cfs_open(FILENAME, CFS_WRITE | CFS_APPEND | CFS_READ);
  if(fd < 0) {
    printf("failed to open %s\n", FILENAME);
    return 0;
  }

  // And initialise the GPS. This must come after the
  // 9600 baud initialisation
  //
  printf("Initialising GPS\n");
	initialise_GPS();

	while (1) {
		printf("Read GPS\n");
		
		FLASH_LED(LEDS_BLUE);

		while (uart2_can_get()) {
			char ch = uart2_getc();
			int r = cfs_write(fd, &ch, 1);
			if (r < 1) {
				printf("failed to write %d bytes to %s\n", 1, FILENAME);
				cfs_close(fd);
				return 0;
			}
		}
		
    //etimer_set(&et, 3*CLOCK_SECOND);
    //PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));
    /*
    int val = 1;

		while(1) {
			if (uart2_can_get()) {
        char ch = uart2_getc();

        if (val == 0) {
				    printf("%c", ch);
				}
        if (val == 1) {
		        if (ch == 0xB5)
		          printf("\n");
						printf("%02X ", ch);
				}
        if (val == 2) {
				    printf("%02X-%c ", ch, ch);
				}
			}	
		}
		*/

	}
  
  PROCESS_END();
}


