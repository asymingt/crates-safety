#include <stdio.h>
#include "contiki.h"
#include "i2c.h"
#include "dev/leds.h"

/*---------------------------------------------------------------------------*/
PROCESS(test_i2c_process, "Test I2C");
AUTOSTART_PROCESSES(&test_i2c_process);
/*---------------------------------------------------------------------------*/

#define CDEL 20
void init(void);
uint8_t readReg(uint8_t, uint8_t);
uint8_t writeReg(uint8_t, uint8_t, uint8_t);


PROCESS_THREAD(test_i2c_process, ev, data)
{ PROCESS_BEGIN();

  //static struct etimer et;

  leds_off(LEDS_ALL);

  init();

  while(1) {
    clock_delay_msec(2000);
		printf("SR: %X\n", *I2CSR);

    //int i;
    //for (i = 0; i < 5; i++) {
    //  printf("Reg = %d\n", readReg(0xD0, 0x01));
		//  clock_delay_msec(1000);
    //}
    //printf("Set time to 27 secs\n");
		//writeReg(0xD0, 0x01, 27);
    //for (i = 0; i < 5; i++) {
    //  printf("Reg = %d\n", readReg(0xD0, 0x01));
		//  clock_delay_msec(1000);
    //}	

    // Wake up device with reset bit in power reg
    //printf("Reg = %d\n", readReg(0x62, 0x6B));

/*
    uint8_t set[] = {0,0} ;

		#define MPU9150_I2C_ADDR			((0xD2)>>1)
		
    set[0] = 0x6B ;
    set[1] = 0x01;        		// You must do this before doing anything else !!
    i2c_transmitinit(MPU9150_I2C_ADDR, 2, set ) ;
    while(!i2c_transferred())

		i2c_transmitinit(MPU9150_I2C_ADDR, 1, set) ;
		while(!i2c_transferred())
		i2c_receiveinit(MPU9150_I2C_ADDR, 1, set) ;
		while(!i2c_transferred())

    printf("Val = %02X\n", set[0]);
*/
    printf("Reset\n");
		writeReg(0xD2, 0x6B, 0x80);
    clock_delay_msec(100);
    printf("Set clock\n");
		writeReg(0xD2, 0x6B, 0x01);
    clock_delay_msec(100);
    printf("Reading reg\n");
    printf("Reg = 0x%02X\n", readReg(0xD2, 0x6B));
    clock_delay_msec(100);
    printf("Reading reg\n");
    printf("Reg = 0x%02X\n", readReg(0xD2, 0x6B));

  }

  PROCESS_END();
}



void init()
{
  ////////////////////////////////////////////////////////////////////////////////
  //
  // Initialisation sequence
  //
  // Set CKEN high - must be done before setting other registers
  //
  // Update I2CFDR and select the required division ratio to obtain the SCL frequency from the platform clock
  // Update I2CADR to define the slave address for this device
  // Modify I2CCR to select master mode, transmit/receive mode and interrupt enable (or not)
  // Set I2CCR[MEN] to enable the I2C interface
  // 

  // Enable clock  
  *I2CCKER = 0x01;

  // Set I2CCR to zero: disable module, diable interrupts, rx mode, send acks, no start, no broadcast accept
  //
  *I2CCR = 0;

  // Now set I2CFDR
  //
  *I2CFDR = 0x20;		// Divider = 160

  // Set the slave address. Not that we use it, but...
  //
  *I2CADR = 0x02;		// Address = 0x02 (bottom bit reserved because of 7 bit addresses)

  // I2CCR set OK for now - not dealine with interrupts, so leave it
  //
  
  // I2CCR master enable
  //
	*I2CCR |= (1 << 7);			// Set MEN so we're now alive.

}

uint8_t readReg(uint8_t dev, uint8_t reg)
{ uint8_t res;

	////////////////////////////////////////////////////////////////////////////////
	// 
	// Generate start sequence
	//
	// If connected to a multi-master system, check status of I2CSR[MBB] to check bus
	//   is free before switching to master mode
	// Select Master mode - I2CCR[MSTA] to transmit serial data and select transmit
	//   mode I2CCR[MTX] for the address cycle
	// Write the slave address into the data register I2CDR.
	//
	// All this assumes that I2C interrupt bit I2CSR[MIF] is cleared. If I2CSR[MIF]=1
	// at any point, we should immediately handle the interrupt.
	//

	////////////////////////////////////////////
	// Check bus is free
	//
  int i;
  for (i = 0; i < 10; i++) {
		if ((*I2CSR & (1 << 5)) != 0) {
			printf("1. Multi master bus not free: SR %02X\n", *I2CSR);
		  clock_delay_msec(500);
		}
    else {
  		printf("1. Bus free, i = %d, SR = %02X\n", i, *I2CSR);
      break;
    }      
  }


	// Check MIF clear
	//
	if ((*I2CSR & (1 << 1)) != 0)
	  printf("1. Interrupt flag set\n");

	// Check MAL clear
	//
	if ((*I2CSR & (1 << 4)) != 0)
	  printf("1. Arbitration lost, SR %02X\n", *I2CSR);
	////////////////////////////////////////////

	// Set MTX to be a transmitter
	//
	*I2CCR |= (1 << 4);

	// Set MSTA to generate START sequence
	//
	*I2CCR |= (1 << 5);

	// Now set the slave address (write)
	//
	*I2CDR = dev; 			// Address of MPU9150, write

	// Wait for the byte to be transferred
	// Test MIF here - both MIF and MCF should be
	// set on the trailing edge of the 9th bit.
	// Data sheet suggests using MIF.
	//
	while ((*I2CSR & (1 << 1)) == 0)
	{
		// Check MAL clear
		//
		if ((*I2CSR & (1 << 4)) != 0)
			printf("2. Arbitration lost, SR %02X\n", *I2CSR);
	}

	// Might need a delay here "to allow I2C signals
	// to settle"
	//
	clock_delay_msec(CDEL);

	// When transfer is complete, MCF should also be set
	// Check this
	//
	if ((*I2CSR & (1 << 7)) == 0)
		printf("3. Transfer complete, but MCF unset\n");
	
	// Clear MIF flag - this must be done in software
	// and, seemingly, after the delay
	//
	*I2CSR &= ~(1<<1);	

	// When transfer is complete, RXAK should be set
	// Check this
	//
	if ((*I2CSR & (1 << 0)) != 0)
		printf("3. Transfer complete, but RXAK unset\n");
	

	////////////////////////////////////////////
	// Check MIF clear
	//
	if ((*I2CSR & (1 << 1)) != 0)
	  printf("4. Interrupt flag set\n");

	// Check MAL clear
	//
	if ((*I2CSR & (1 << 4)) != 0)
	  printf("4. Arbitration lost, SR %02X\n", *I2CSR);
	////////////////////////////////////////////

	// Put register address
	//
	*I2CDR = reg; 			// Address of reg

	// Wait for the byte to be transferred
	// Test MIF here - both MIF and MCF should be
	// set on the trailing edge of the 9th bit.
	// Data sheet suggests using MIF.
	//
	while ((*I2CSR & (1 << 1)) == 0)
	{
		// Check MAL clear
		//
		if ((*I2CSR & (1 << 4)) != 0)
			printf("5. Arbitration lost, SR %02X\n", *I2CSR);
	}

	// Might need a delay here "to allow I2C signals
	// to settle"
	//
	clock_delay_msec(CDEL);

	// When transfer is complete, MCF should also be set
	// Check this
	//
	if ((*I2CSR & (1 << 7)) == 0)
		printf("6. Transfer complete, but MCF unset\n");

	// Clear MIF flag - this must be done in software
	// and, seemingly, after the delay
	//
	*I2CSR &= ~(1<<1);	

	// When transfer is complete, RXAK should be set
	// Check this
	//
	if ((*I2CSR & (1 << 0)) != 0)
		printf("6. Transfer complete, but RXAK unset\n");
	

	////////////////////////////////////////////
	// Now generate repeated start
	//
	// Set RSTA to generate START sequence
	//
	*I2CCR |= (1 << 2);

	////////////////////////////////////////////  
	// And put slave address again (but read this time)
	//
	*I2CDR = dev + 1; 			// Address of RTC, read

	// Wait for the byte to be transferred
	// Test MIF here - both MIF and MCF should be
	// set on the trailing edge of the 9th bit.
	// Data sheet suggests using MIF.
	//
	while ((*I2CSR & (1 << 1)) == 0)
	{
		// Check MAL clear
		//
		if ((*I2CSR & (1 << 4)) != 0)
			printf("7. Arbitration lost, SR %02X\n", *I2CSR);
	}

	// Might need a delay here "to allow I2C signals
	// to settle"
	//
	//clock_delay_msec(CDEL);

	// When transfer is complete, MCF should also be set
	// Check this
	//
	if ((*I2CSR & (1 << 7)) == 0)
		printf("7. Transfer complete, but MCF unset\n");

	// Clear MIF flag - this must be done in software
	// and, seemingly, after the delay
	//
	*I2CSR &= ~(1<<1);	

	// When transfer is complete, RXAK should be set
	// Check this
	//
	if ((*I2CSR & (1 << 0)) != 0)
		printf("8. Transfer complete, but RXAK unset\n");
	
	////////////////////////////////////////////  
	//
	// Set MTX to be a receiver
	//
	*I2CCR &= ~(1 << 4);
 
	////////////////////////////////////////////  
	// Set NACK, as next to last byte
	//
	*I2CCR |= (1 << 3);

	////////////////////////////////////////////  
	// Read value - should be outgoing register,
	// so this is a dummy read
	//
	printf("Value(1) is %02X\n", *I2CDR);

	// Wait for the byte to be transferred
	// Test MIF here - both MIF and MCF should be
	// set on the trailing edge of the 9th bit.
	// Data sheet suggests using MIF.
	//
	while ((*I2CSR & (1 << 1)) == 0)
	{ // Check MAL clear
		//
		if ((*I2CSR & (1 << 4)) != 0)
			printf("9. Arbitration lost, SR %02X\n", *I2CSR);
	}

	// Might need a delay here "to allow I2C signals
	// to settle"
	//
	clock_delay_msec(CDEL);

	// When transfer is complete, MCF should also be set
	// Check this
	//
	if ((*I2CSR & (1 << 7)) == 0)
		printf("10. Transfer complete, but MCF unset\n");

	*I2CSR &= ~(1<<1);	// Clear MIF flag

	////////////////////////////////////////////
	// Check MIF clear
	//
	if ((*I2CSR & (1 << 1)) != 0)
	  printf("11. Interrupt flag set\n");

	// Check MAL clear
	//
	if ((*I2CSR & (1 << 4)) != 0)
	  printf("11. Arbitration lost, SR %02X\n", *I2CSR);
	////////////////////////////////////////////


	////////////////////////////////////////////  
	// Read value - should be what we want
	//
  res = *I2CDR;
	printf("Value(2) is %02X\n", res);

	while ((*I2CSR & (1 << 1)) == 0)
	{ // Check MAL clear
		//
		if ((*I2CSR & (1 << 4)) != 0)
			printf("9. Arbitration lost, SR %02X\n", *I2CSR);
	}

	// Might need a delay here "to allow I2C signals
	// to settle"
	//
	clock_delay_msec(CDEL);

	*I2CSR &= ~(1<<1);	// Clear MIF flag
	// And we must clear TXAK
	*I2CCR &= ~(1 << 3);

	////////////////////////////////////////////
	// Check MIF clear
	//
	if ((*I2CSR & (1 << 1)) != 0)
	  printf("13. Interrupt flag set\n");

	// Check MAL clear
	//
	if ((*I2CSR & (1 << 4)) != 0)
	  printf("13. Arbitration lost, SR %02X\n", *I2CSR);
	////////////////////////////////////////////


	////////////////////////////////////////////  

  // That should have sent a STOP anyway, but drop MSTA
	// Unset MSTA to generate STOP sequence
	//
	*I2CCR &= ~(1 << 5);

  return res;
}

uint8_t writeReg(uint8_t dev, uint8_t reg, uint8_t val)
{
	////////////////////////////////////////////////////////////////////////////////
	// 
	// Generate start sequence
	//
	// If connected to a multi-master system, check status of I2CSR[MBB] to check bus
	//   is free before switching to master mode
	// Select Master mode - I2CCR[MSTA] to transmit serial data and select transmit
	//   mode I2CCR[MTX] for the address cycle
	// Write the slave address into the data register I2CDR.
	//
	// All this assumes that I2C interrupt bit I2CSR[MIF] is cleared. If I2CSR[MIF]=1
	// at any point, we should immediately handle the interrupt.
	//

	////////////////////////////////////////////
	// Check bus is free
	//
  int i;
  for (i = 0; i < 10; i++) {
		if ((*I2CSR & (1 << 5)) != 0) {
			printf("1. Multi master bus not free: SR %02X\n", *I2CSR);
		  clock_delay_msec(500);
		}
  }
	// Check MIF clear
	//
	if ((*I2CSR & (1 << 1)) != 0)
	  printf("1. Interrupt flag set\n");

	// Check MAL clear
	//
	if ((*I2CSR & (1 << 4)) != 0)
	  printf("1. Arbitration lost, SR %02X\n", *I2CSR);
	////////////////////////////////////////////

	// Set MTX to be a transmitter
	//
	*I2CCR |= (1 << 4);

	// Set MSTA to generate START sequence
	//
	*I2CCR |= (1 << 5);

	// Now set the slave address (write)
	//
	*I2CDR = dev; 			// Address of MPU9150, write

	// Wait for the byte to be transferred
	// Test MIF here - both MIF and MCF should be
	// set on the trailing edge of the 9th bit.
	// Data sheet suggests using MIF.
	//
	while ((*I2CSR & (1 << 1)) == 0)
	{
		// Check MAL clear
		//
		if ((*I2CSR & (1 << 4)) != 0)
			printf("2. Arbitration lost, SR %02X\n", *I2CSR);
	}

	// Might need a delay here "to allow I2C signals
	// to settle"
	//
	clock_delay_msec(CDEL);

	// When transfer is complete, MCF should also be set
	// Check this
	//
	if ((*I2CSR & (1 << 7)) == 0)
		printf("3. Transfer complete, but MCF unset\n");
	
	// Clear MIF flag - this must be done in software
	// and, seemingly, after the delay
	//
	*I2CSR &= ~(1<<1);	

	// When transfer is complete, RXAK should be set
	// Check this
	//
	if ((*I2CSR & (1 << 0)) != 0)
		printf("3. Transfer complete, but RXAK unset\n");
	

	////////////////////////////////////////////
	// Check MIF clear
	//
	if ((*I2CSR & (1 << 1)) != 0)
	  printf("4. Interrupt flag set\n");

	// Check MAL clear
	//
	if ((*I2CSR & (1 << 4)) != 0)
	  printf("4. Arbitration lost, SR %02X\n", *I2CSR);
	////////////////////////////////////////////

	// Put register address
	//
	*I2CDR = reg; 			// Address of reg

	// Wait for the byte to be transferred
	// Test MIF here - both MIF and MCF should be
	// set on the trailing edge of the 9th bit.
	// Data sheet suggests using MIF.
	//
	while ((*I2CSR & (1 << 1)) == 0)
	{
		// Check MAL clear
		//
		if ((*I2CSR & (1 << 4)) != 0)
			printf("5. Arbitration lost, SR %02X\n", *I2CSR);
	}

	// Might need a delay here "to allow I2C signals
	// to settle"
	//
	clock_delay_msec(CDEL);

	// When transfer is complete, MCF should also be set
	// Check this
	//
	if ((*I2CSR & (1 << 7)) == 0)
		printf("6. Transfer complete, but MCF unset\n");

	// Clear MIF flag - this must be done in software
	// and, seemingly, after the delay
	//
	*I2CSR &= ~(1<<1);	

	// When transfer is complete, RXAK should be set
	// Check this
	//
	if ((*I2CSR & (1 << 0)) != 0)
		printf("6. Transfer complete, but RXAK unset\n");
	

	// Now send the value to be written to the reg.
	//
	*I2CDR = val;

	// Wait for the byte to be transferred
	// Test MIF here - both MIF and MCF should be
	// set on the trailing edge of the 9th bit.
	// Data sheet suggests using MIF.
	//
	while ((*I2CSR & (1 << 1)) == 0)
	{
		// Check MAL clear
		//
		if ((*I2CSR & (1 << 4)) != 0)
			printf("7. Arbitration lost, SR %02X\n", *I2CSR);
	}

	// Might need a delay here "to allow I2C signals
	// to settle"
	//
	clock_delay_msec(CDEL);

	// When transfer is complete, MCF should also be set
	// Check this
	//
	if ((*I2CSR & (1 << 7)) == 0)
		printf("8. Transfer complete, but MCF unset\n");
	
	// Clear MIF flag - this must be done in software
	// and, seemingly, after the delay
	//
	*I2CSR &= ~(1<<1);	

	// When transfer is complete, RXAK should be set
	// Check this
	//
	if ((*I2CSR & (1 << 0)) != 0)
		printf("8. Transfer complete, but RXAK unset\n");
	

	////////////////////////////////////////////
	// Check MIF clear
	//
	if ((*I2CSR & (1 << 1)) != 0)
	  printf("9. Interrupt flag set\n");

	// Check MAL clear
	//
	if ((*I2CSR & (1 << 4)) != 0)
	  printf("9. Arbitration lost, SR %02X\n", *I2CSR);
	////////////////////////////////////////////


  // That should have sent a STOP anyway, but drop MSTA
	// Unset MSTA to generate STOP sequence
	//
	*I2CCR &= ~(1 << 5);
}

/*---------------------------------------------------------------------------*/
