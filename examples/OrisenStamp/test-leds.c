#include "contiki.h"
#include <stdio.h>
#include "dev/leds.h"

PROCESS(LED_test_process, "LED test process");
AUTOSTART_PROCESSES(&LED_test_process);

#define FLASH_LED(l) {leds_on(l); clock_delay_msec(50); leds_off(l); clock_delay_msec(50);}

PROCESS_THREAD(LED_test_process, ev, data)
{
  static struct etimer et;
  PROCESS_BEGIN();

  while (1) {
    printf("Hello world\n");
    
    FLASH_LED(LEDS_BLUE);
    FLASH_LED(LEDS_GREEN);
    FLASH_LED(LEDS_YELLOW);

    etimer_set(&et, 3*CLOCK_SECOND);
    PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));
  }
  
  PROCESS_END();
}

