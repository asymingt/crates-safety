#include "contiki.h"
#include <stdio.h>

PROCESS(hello_world_process, "Hello world process");
AUTOSTART_PROCESSES(&hello_world_process);

PROCESS_THREAD(hello_world_process, ev, data)
{
  static struct etimer et;
  PROCESS_BEGIN();

	while (1) {
		printf("Hello world\n");
		
    etimer_set(&et, 3*CLOCK_SECOND);
    PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));
	}
  
  PROCESS_END();
}

