/*
 * test_code.c
 *
 *  Created on: 4 Sep 2013
 *      Author: mcphillips
 */


#include <mc1322x.h>
#include <board.h>
#include <stdio.h>
#include "software_defines.h"

//#include "tests.h"
#define MPU9150 1
#include "tmr_.h"
#if MPU9150
#include "mpu9150.h"
#endif

#if MS5611_01BA03
#include "ms5611-01ba03.h"
#endif

#if M41T62
#include "m41t62.h"
#endif

#define SD_CARD	0

volatile uint8_t kbi4_flag;
volatile uint8_t kbi7_flag;
struct mpu9150_data imu_data;
struct mpu9150_data imu_fifo_data;
uint8_t imu_data_buffer[15];
uint8_t fifo_count,fifo_count_a;
uint8_t fifo_buffer_count[2],imu_fifo_data_buffer[15];
char imu_str[60];
struct mpu9150_mag_data mag_data;
uint8_t mag_data_buffer[7];
char mag_str[60];

volatile uint8_t led,led2;
volatile uint8_t grab;
volatile uint8_t delay;
volatile uint8_t r,m,g,tmr1_count;

void tmr0_init(void) {
	*TMR_ENBL     &= ~(TMR0_EN);                    /* tmrs reset to enabled */
	*TMR0_SCTRL   = 0;
	*TMR0_CSCTRL  = 0x0040;
	*TMR0_LOAD    = 0;                    /* reload to zero */
//	*TMR0_COMP_UP = 1875;                /* trigger a reload at the end */
//	*TMR0_CMPLD1  = 1875;                /* compare 1 triggered reload level, 100Hz */
	*TMR0_COMP_UP = 37500;                /* trigger a reload at the end */
	*TMR0_CMPLD1  = 37500;                /* compare 1 triggered reload level, 5Hz */
	*TMR0_CNTR    = 0;                    /* reset count register */
	*TMR0_CTRL    = (COUNT_MODE0<<13) | (PRIME_SRC0<<9) | (SEC_SRC0<<7) | (ONCE0<<6) | (LEN0<<5) | (DIR0<<4) | (CO_INIT0<<3) | (OUT_MODE0);
	*TMR_ENBL     |= TMR0_EN;                  /* enable tmr0 */
}

void tmr1_init(void) {
	*TMR_ENBL     &= ~(TMR1_EN);                    /* tmrs reset to enabled */
	*TMR1_SCTRL   = 0;
	*TMR1_CSCTRL  = 0x0040;
	*TMR1_LOAD    = 0;                    /* reload to zero */
//	*TMR1_COMP_UP = 7500;                /* trigger a reload at the end */
//	*TMR1_CMPLD1  = 7500;                /* compare 1 triggered reload level, 25Hz */
//	*TMR1_COMP_UP = 37500;                /* trigger a reload at the end */
//	*TMR1_CMPLD1  = 37500;                /* compare 1 triggered reload level, 5Hz */
	*TMR1_COMP_UP = 1875;                /* trigger a reload at the end */
	*TMR1_CMPLD1  = 1875;                /* compare 1 triggered reload level, 100Hz */
	*TMR1_CNTR    = 0;                    /* reset count register */
	*TMR1_CTRL    = (COUNT_MODE1<<13) | (PRIME_SRC1<<9) | (SEC_SRC1<<7) | (ONCE1<<6) | (LEN1<<5) | (DIR1<<4) | (CO_INIT1<<3) | (OUT_MODE1);
	*TMR_ENBL     |= TMR1_EN;                  /* enable tmr1 */
}

void tmr0_isr(void) {
	if (*TMR0_SCTRL & TCF) {
		delay++;
		if (led){
			LED1_ON;
			led = 0;
			grab = 1;
		}else{
			LED1_OFF;
			led = 1;
		}
		*TMR0_SCTRL &= ~TCF;
		*TMR0_CSCTRL = *TMR0_CSCTRL & ~(TCF1);
	}
}

void tmr1_isr(void) {
	if (*TMR1_SCTRL & TCF){
//		if (led2){
//			LED2_ON;
//			led2 = 0;
//			grab = 1;
//		}else{
//			LED2_OFF;
//			led2 = 1;
//		}
		if (tmr1_count < 250)tmr1_count++;
		*TMR1_SCTRL &= ~TCF;
		*TMR1_CSCTRL = *TMR1_CSCTRL & ~(TCF1);
	}
}

void led_init(void) {
	GPIO->PAD_DIR.LED1 = 1;
	LED1_OFF;
#if LED2_P
	GPIO->PAD_DIR.LED2 = 1;
	LED2_ON;
#endif
}

#if SD_CARD
void sd_card_switch_init(void) {
	GPIO->FUNC_SEL.SD_CARD_SWITCH = SD_CARD_INIT_FUNC_SEL;
	GPIO->PAD_DIR.SD_CARD_SWITCH = 0;
	GPIO->PAD_PU_SEL.SD_CARD_SWITCH = 1;
	GPIO->PAD_PU_EN.SD_CARD_SWITCH = 1;
	GPIO->PAD_HYST_EN.SD_CARD_SWITCH = 1;
#if SD_POWER_CTL
	GPIO->PAD_DIR.SD_CARD_PWR = 1;
	SD_CARD_OFF;
#endif
}
#endif

void kbi7_init(void) {
	kbi_edge(7);
	enable_ext_wu(7);
	kbi_pol_neg(7);
	GPIO->PAD_DIR.KBI_7 = 0;
	GPIO->PAD_PU_SEL.KBI_7 = 1;
	GPIO->PAD_PU_EN.KBI_7 = 1;
	GPIO->PAD_HYST_EN.KBI_7 = 1;
	clear_kbi_evnt(7);
}

void kbi4_init(void) {
	kbi_edge(4);
	enable_ext_wu(4);
	kbi_pol_neg(4);
	GPIO->PAD_DIR.KBI_4 = 0;
	GPIO->PAD_PU_SEL.KBI_4 = 1;
	GPIO->PAD_PU_EN.KBI_4 = 1;
	GPIO->PAD_HYST_EN.KBI_4 = 1;
	clear_kbi_evnt(4);
}

void kbi4_isr(void) {

	kbi4_flag = 1;
//	PRINTF("Gone into KBI 4\n") ;
//	toggle_led4();
	clear_kbi_evnt(4);
}

void kbi7_isr(void) {
	if (GPIO->DATA.INT_MPU == 0) m++;
	kbi7_flag = 1;
//	PRINTF("Gone into KBI 7\n") ;
//	r = r + GPIO->DATA.INT_RTC;
//	if (!GPIO->DATA.GPS_TIMEPULSE) g++;
//	LED4_ON;
	clear_kbi_evnt(7);
}

void main(void) {

	trim_xtal();	/* trim the reference osc. to 24MHz */
	vreg_init();


	uart_init(UART1, 115200);

	tmr0_init();	/* timer setup */
	tmr1_init();	/* timer setup */
	led_init();
	kbi7_init();	/* io bus setup */
	kbi4_init();	/* push button setup */

	GPIO->FUNC_SEL.INT_MPU = 1;
	GPIO->PAD_DIR.INT_MPU = 0;
	GPIO->PAD_PU_SEL.INT_MPU = 0;
	GPIO->PAD_PU_EN.INT_MPU = 0;
	GPIO->PAD_HYST_EN.INT_MPU = 0;

//	GPIO->FUNC_SEL.INT_RTC = 1;
	GPIO->FUNC_SEL_50 = 1;
	GPIO->PAD_DIR.INT_RTC = 0;
	GPIO->PAD_PU_SEL.INT_RTC = 0;
	GPIO->PAD_PU_EN.INT_RTC = 0;
	GPIO->PAD_HYST_EN.INT_RTC = 0;

	GPIO->FUNC_SEL.GPS_TIMEPULSE = 1;
	GPIO->PAD_DIR.GPS_TIMEPULSE = 0;
	GPIO->PAD_PU_SEL.GPS_TIMEPULSE = 0;
	GPIO->PAD_PU_EN.GPS_TIMEPULSE = 0;
	GPIO->PAD_HYST_EN.GPS_TIMEPULSE = 0;

#if SD_CARD
	sd_card_switch_init();
#endif

	enable_irq(TMR);	/* start various interrupts */
	enable_irq_kbi(4);
	enable_irq_kbi(7);
	enable_irq(CRM);

	*I2CCR |= I2C_RSTA;
	i2c_enable();

	PRINTF("Hello World\n");

#if MPU9150
	uint8_t who_am_i;
	mpu9150_init();
	mpu9150_who_am_i(&who_am_i);
	PRINTF("mpu9150 - Who Am I: 0x%X\n", who_am_i);

	mpu9150_mag_who_am_i(&who_am_i);
	PRINTF("mpu9150 mag - Who Am I: 0x%X\n", who_am_i);

#endif

#if MS5611_01BA03
	ms5611_01ba03_reset();
	PRINTF("Reset MS5611-01BA03\n");
#endif

#if M41T62
	uint8_t flags;

	m41t62_init();
	m41t62_flags(&flags);
	PRINTF("m41t62 Flags: 0x%X\n", flags);


	m41t62_watchdog(&flags);
	PRINTF("m41t62 watchdog: 0x%X\n", flags);
	m41t62_minutes(&flags);
	PRINTF("m41t62 minutes: 0x%X\n", flags);
	m41t62_al_month(&flags);
	PRINTF("m41t62 al month: 0x%X\n", flags);


#endif


	delay = 0;

//	while(delay <= 10){}
//
//	PRINTF("Reconfig UART\n");
//
//	while(delay <= 20){}

	GPIO->PAD_PU_SEL.U2RX = 1;
//	uart_init(UART1, 9600);
	uart_init(UART2, 9600);

	mpu9150_user_ctrl();

	while(1) {

//		if (kbi7_flag) {
//			PRINTF("I ") ;
//			if (!r) PRINTF("R ");
////			if (!m) PRINTF("M ");
//			if (!g) PRINTF("G");
//			PRINTF("\n") ;
//			kbi7_flag = 0;
//		}

		if (!r) {
			PRINTF("R\n");
			r = 1;
		}

		if (g) {
//			PRINTF("GPS DATA\n");
//			delay = 0;
//			while (delay < 3){};
//			while(uart2_can_get()) {
//				uart1_putc(uart2_getc());
//			}
			PRINTF("G,%d\n",g);
			g = 0;
		}

//		if (m){
		if (!(GPIO->DATA.INT_MPU) /*&& tmr1_count > 10*/){
//			tmr1_count = 0;
//			PRINTF("M,%d\n",m);
			mpu9150_mag_start();
			mpu9150_get_data(imu_data_buffer);
			mpu9150_decode(&imu_data, imu_data_buffer);
			sprintf(imu_str,"imu direct: acc x %d,acc y %d,acc z %d,temp %d,gyro x %d,gyro y %d,gyro z %d\r", imu_data.acc_x, imu_data.acc_y, imu_data.acc_z, imu_data.temp, imu_data.gyro_x, imu_data.gyro_y, imu_data.gyro_z) ;
			PRINTF(imu_str);
			mpu9150_get_fifo_count(fifo_buffer_count);
			fifo_count = (((fifo_buffer_count[0]<<8) & 0x07) + fifo_buffer_count[1]);
//			while (fifo_count > 0) {
			mpu9150_get_fifo(imu_fifo_data_buffer);
			mpu9150_get_fifo_count(fifo_buffer_count);
			fifo_count_a = (((fifo_buffer_count[0]<<8) & 0x07) + fifo_buffer_count[1]);
			mpu9150_decode(&imu_fifo_data, imu_fifo_data_buffer);
			sprintf(imu_str,"imu fifo: count before %d, count after %d,acc x %d,acc y %d,acc z %d,temp %d,gyro x %d,gyro y %d,gyro z %d\r", fifo_count, fifo_count_a, imu_fifo_data.acc_x, imu_fifo_data.acc_y, imu_fifo_data.acc_z, imu_fifo_data.temp, imu_fifo_data.gyro_x, imu_fifo_data.gyro_y, imu_fifo_data.gyro_z) ;
			PRINTF(imu_str);

			mpu9150_mag_get_data(mag_data_buffer);
			mpu9150_mag_decode(&mag_data, mag_data_buffer);
			sprintf(mag_str,"mag direct: mag x %d,mag y %d,mag z %d\r", mag_data.mag_x, mag_data.mag_y, mag_data.mag_z) ;
			PRINTF(mag_str);




//			}
//			kbi7_flag = 0;
//			m = 0;
		}
//			if ((GPIO->DATA.INT_RTC = 0)){
//				PRINTF("INT_RTC = 0\n");
//			}else PRINTF("INT_RTC = 1\n");
//			if ((GPIO->DATA.KBI_7 = 0)){
//				PRINTF("KBI_7 = 0\n");
//			}else PRINTF("KBI_7 = 1\n");
//				PRINTF("GPS_TIMEPULSE = 0\n");
//			}else PRINTF("GPS_TIMEPULSE = 1\n");

//		}
//		if ((GPIO->DATA.INT_MPU = 0)){
//			PRINTF("INT_MPU = 0\n");
//			grab = 1;
//		}


//		if(uart1_can_get()) {
//			/* Receive buffer isn't empty */
//			/* read a byte and write it to the transmit buffer */
//			uart2_putc(uart1_getc());
//		}
//		if(uart2_can_get()) {
//			uart1_putc(uart2_getc());
//		}
	};

}






