/*
 * Copyright (c) 2013, Dominik Beste <dbeste@cs.ucl.ac.uk>
 *
 * Driver for the MPU9150 chip used on the OrisenSpectrum.
 */

#ifndef TEST_H_
#define TEST_H_

#include "i2c.h"

int find_next_filename(void);
int open_file(uint16_t fnum, char* ext);
int open_next_file(int lastFile, int* name, char* ext);

#define KBI_7		GPIO_29

void kbi7_init(void);
//void kbi7_isr(void);
void sd_card_switch_init(void);



#endif 
