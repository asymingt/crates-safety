/**
 * \file
 *         Test stuff for OrisenSpecrum
 * \author
 *         Adam Dunkels <adam@sics.se>
 */

#include "contiki.h"
#include "dev/leds.h"
#include "lib/sensors.h"
#include "string.h"

#include "mpu9150.h"
#include "kbi.h"
#include "cfs/cfs.h"
#include "uart.h"
#include "GPS.h"

#include "test-graeme.h"


#define GPS_NMEA_BUFFER_LENGTH 255

int idx = 0;
struct mpu9150_data imu_data;
struct mpu9150_data imu_fifo_data;
struct mpu9150_int_data int_status;
uint8_t fifo_read_flag;
uint8_t imu_data_buffer[15];
uint8_t imu_fifo_data_buffer[MPU_9150_FIFO_COUNT_MAX];
uint8_t packet_size = 14;
uint16_t imu_fifo_max_read = 126;
uint16_t fifo_count;
uint16_t fifo_count_whole_records;
uint16_t fifo_read_length;
uint8_t int_status_buffer[2];
uint8_t gps_nmea_buffer[GPS_NMEA_BUFFER_LENGTH];
volatile uint8_t kbi7_flag;


int find_next_filenum(void) {
    struct cfs_dir dir;
    struct cfs_dirent dirent;
    uint16_t max = 0;
    // Find highest numbered file name and then make new filename that +1
    int res = cfs_opendir(&dir, "/");
    int i = 0;
    if (res  ==  0) {
        while(cfs_readdir(&dir, &dirent) != -1) {
            uint16_t tmpInt = atoi(dirent.name);
            //printf("%4d: Filename: %s (res=%i)\n", i++,dirent.name,res);
            if (tmpInt > max) {
                max = tmpInt;
            }
        }
    } else {
        printf("ERROR: Cannot open root directory. Check if SD card is formatted with FAT.\n");
        return -1;
    }
    return max + 1;
}

int open_file(uint16_t fnum, char* ext) {
	/* cfs filenames can be 31 chars (plus null termination) but we are using cfs-fat, 
	 * which uses 8.3 filenames (8 char name, 3 char extension - 13 chars incl null 
	 * termination). Therefore the maximum file number we can store is 99999999 */
	if (fnum > 99999999) {
		printf("ERROR: Out of file numbers (max: 99,999,999)");
		return -1;
	}
    char str[32]; 
    sprintf(str, "%d.%s", fnum,ext);
    int fid = cfs_open(str, CFS_WRITE | CFS_APPEND);
    return fid;
}

int open_next_file(int lastFile, int* name, char* ext){
    cfs_close(lastFile);
    (*name)++;
    return open_file(*name, ext); 
}

void kbi7_init(void){
	GPIO->PAD_DIR.KBI_7 = 0;
	//GPIO->PAD_PU_SEL.KBI_7 = 1;
	//GPIO->PAD_PU_EN.KBI_7 = 1;
	GPIO->PAD_HYST_EN.KBI_7 = 0;
	kbi_edge(7);
	enable_ext_wu(7);
	kbi_pol_neg(7);
	clear_kbi_evnt(7);
}

//ISR(KBI7, kbi7_isr){
void kbi7_isr(void){
	kbi7_flag = 1;
	printf("kbi7 triggered.\n");
	clear_kbi_evnt(7);
}

/*---------------------------------------------------------------------------*/
PROCESS(test_process, "Test");
AUTOSTART_PROCESSES(&test_process);
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
PROCESS_THREAD(test_process, ev, data) {
    static struct etimer et;

    PROCESS_BEGIN();
    printf("clock second %i\n", CLOCK_SECOND);
    printf("Setting up interrupts...");
    // Set up interrupts
    kbi7_init(); // Combined RTC, MPU and GSP interrupt
    //enable_irq(TMR);
    enable_irq_kbi(7);
    //enable_irq(CRM);
    printf("Initial kbi7 interrupt %i (MPU=%i; GPS=%i);\n",idx,!(GPIO->DATA.INT_MPU),!(GPIO->DATA.GPS_TIMEPULSE));
    
    printf("Activating sensors...\n");
    SENSORS_ACTIVATE(mpu9150_sensor);
    initialise_GPS(); // Initialise GPS
    printf("After SENSORS_ACTIVATE kbi7 interrupt %i (MPU=%i; GPS=%i);\n",idx,!(GPIO->DATA.INT_MPU),!(GPIO->DATA.GPS_TIMEPULSE));
    
    // Now get timepulse configuration to see if time pulse length is too long,
    // causing the interrupt to always be high:
    char cfg_buf[255];
    //get_CFG_TP(cfg_buf);
    printf("CFG_BUF: %s", cfg_buf);
    
    
    // Initialise storage and logging
    printf("Initialising SD card...\n");
    uSDcard_init(); // Power up and mount SD card
    // Ensure SD card is present so data can be logged
    if (GPIO->DATA.SD_CARD_SWITCH) { // 0: present; 1:missing
    	printf("ERROR: No SD card present.");
    	return;
    }		
    // Get lowest number not already used for data log file names
    printf("Getting first file number...");
    int fnum;
    fnum = find_next_filenum();
    if (fnum == -1) {
    	printf("ERROR: Failed to scan filesystem.");
    	return;
    }
    
    printf("First file number is %i\n",fnum);
    printf("About to call mpu9150_who_am_i()...\n");
    uint8_t who_am_i_buffer[1];
    mpu9150_who_am_i(who_am_i_buffer); //(&who_am_i_buffer)
    // WhoAmI retuns the value we are interested in in bits [6:1],
    // so we need to zero out bits 7 and 0 and shift to left by one:
    uint8_t mask = 0b01111110;
    uint8_t who_am_i_trimmed = *who_am_i_buffer & mask;
    uint8_t who_am_i_shifted = lsr(who_am_i_trimmed, 0);
    printf("WHO_AM_I value: %i\n", who_am_i_shifted);

    //etimer_set(&et, 3*CLOCK_SECOND);
    //PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));

    printf("clock second %i\n", CLOCK_SECOND);

    /* Set up sensors */
    uint8_t cmd[] = {MPU9150_PWR_MGMT_1, 0x01};
    i2c_transmitinit(MPU9150_I2C_ADDR, 2, cmd);
    while (!i2c_transferred()) /* Wait for transfer */;

    /* Set up interrupt for MPU */
    //kbi7_init(); /* io bus setup */

    GPIO->FUNC_SEL.INT_MPU = 1;
    GPIO->PAD_DIR.INT_MPU = 0;
    GPIO->PAD_PU_SEL.INT_MPU = 0;
    GPIO->PAD_PU_EN.INT_MPU = 0;
    GPIO->PAD_HYST_EN.INT_MPU = 0;

    /*** Gyroscope ***/
    printf("Setting up gyroscope...\n");
    //Self test xyz and set full scale range to +-2000s:
    uint8_t gyro_config = 0b11101000;
    cmd[0] = MPU9150_GYRO_CONFIG;
    cmd[1] = gyro_config;
    i2c_transmitinit(MPU9150_I2C_ADDR, 2, cmd);
    while (!i2c_transferred()) /* Wait for transfer */;
    printf("Gyroscope started!\n");

    mpu9150_user_ctrl(); // Reset FIFO buffer

    // Open first file for this run:
    int GPS_OFFSET = 1000;
    int fid = open_file(fnum,"dat");
    //int fid2 = open_file(fnum,"gps"); // Can't open two files at once with CFS
    mpu9150_user_ctrl(); // Reset FIFO
    double minutes = 0.1;
    while (true) {
    	idx++;
    	if((GPIO->DATA.KBI_7)){
    		printf("iter %i: KBI7 is high!!!",idx);
                printf("kbi7 interrupt %i (MPU=%i; GPS=%i);\n",idx,!(GPIO->DATA.INT_MPU),!(GPIO->DATA.GPS_TIMEPULSE));
    	}
    	if((GPIO->DATA.GPS_TIMEPULSE)){
    		printf("iter %i: GPS is high!!!",idx);
                printf("kbi7 interrupt %i (MPU=%i; GPS=%i);\n",idx,!(GPIO->DATA.INT_MPU),!(GPIO->DATA.GPS_TIMEPULSE));
    	} else {
            //printf("iter %i: GPS is LOW!!!!!!!!!!!!!!!!!!!!!!!\n",idx);
        }
        //etimer_set(&et, 1*CLOCK_SECOND);
        //PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));
    	//printf("int=%i, (KBI7=%i,MPU=%i; GPS=%i; kbi_evnt=%i);\n",kbi7_flag,\
    		!(GPIO->DATA.KBI_7),!(GPIO->DATA.INT_MPU),!(GPIO->DATA.GPS_TIMEPULSE),kbi_evnt(7));
    		
        if (kbi7_flag){
			//idx++;
    		printf("kbi7 interrupt %i (MPU=%i; GPS=%i);\n",idx,!(GPIO->DATA.INT_MPU),!(GPIO->DATA.GPS_TIMEPULSE));
    		kbi7_flag = 0;
			if (!(GPIO->DATA.INT_MPU) /*&& tmr1_count > 10*/) {
				//if (kbi7_flag) { // kbi7_flag defined in contiki-spectrum-main.c
				//if (1){

				//mpu9150_get_int_status(int_status_buffer);
				//mpu9150_decode_int(&int_status, int_status_buffer);
				//printf("%4d: INT status before FIFO read: FO: %i, MST: %i, DATA: %i; [%i,%i];\n",\
					idx, int_status.fifo_oflow, int_status.mst, int_status.data_rdy,\
					int_status_buffer[0], int_status_buffer[1]);


				//printf("--------------------------- Iteration #%i ---------------------------\n", idx++);
			
				mpu9150_get_fifo_count(&fifo_count);
				fifo_count_whole_records = fifo_count - (fifo_count % packet_size);
				if (fifo_count_whole_records < packet_size) {
					printf("%4d: FIFO too small! (%i [w=%i])\n", idx, fifo_count, fifo_read_length);
					continue;
				}
				fifo_read_flag = 1;
				while(fifo_read_flag) {
					if (fifo_count_whole_records > imu_fifo_max_read) {
						fifo_read_length = imu_fifo_max_read;
						fifo_read_flag = 1;
					} else {
						fifo_read_length = fifo_count_whole_records;
						fifo_read_flag = 0;
					}
					if (fifo_read_length>packet_size){
						//printf("Multiple records: fifo count: %i (whole records: %i; fifo_read_length: %i)\n", \
							fifo_count, fifo_count_whole_records, fifo_read_length);
					}
					//
					/* Read FIFO */
					mpu9150_get_fifo(imu_fifo_data_buffer, fifo_read_length);
					fifo_count_whole_records = fifo_count_whole_records - fifo_read_length;
					// Write data to file
					//cfs_write(fid, imu_fifo_data_buffer, fifo_read_length);
				}
				if (!(GPIO->DATA.GPS_TIMEPULSE)){
					gps_nmea_buffer[0] = '\0';
					strcpy(gps_nmea_buffer,"\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0");
					//printf("Initialised buffer length: %i\n",strlen(gps_nmea_buffer));
					while(uart2_can_get()){
						uart1_putc(uart2_getc());
					}
// 					if(amy_6m_get_nmea_msg(gps_nmea_buffer)){
// 						printf("%i: nmea: %s (%i)\n",idx,gps_nmea_buffer,strlen(gps_nmea_buffer));
// 						cfs_write(fid, gps_nmea_buffer, strlen(gps_nmea_buffer)); // write to same file as MPU
// 					}
// 					else {
// 						//printf("%i: nmea FAIL\n",idx);
// 					}
				}
			}
		}
    }
    printf("Out of while loop.");
    
    // Close current data log file
    cfs_close(fid);  
    //cfs_close(fid2); No longer opening two files 

    PROCESS_END();
}

//void writeBufferToSD(uint8_t* buffer, uint16_t num, *) {
//    
//}
/*---------------------------------------------------------------------------*/


/*
 * Function to return the WHO_AM_I value
 * of the mpu9150 chip.
 */
/*
void mpu9150_who_am_i(uint8_t *buffer) {
   //uint8_t set[] = {MPU9150_WHOAMI,0};

   uint8_t cmd = 0;

   cmd = MPU9150_WHOAMI;

   //i2c_transmitinit( MPU9150_I2C_ADDR, 1, &set );
   i2c_transmitinit( MPU9150_I2C_ADDR, 1, &cmd );
   while(!i2c_transferred());

   i2c_receiveinit( MPU9150_I2C_ADDR, 1, buffer );
   while(!i2c_transferred());
}
 */
