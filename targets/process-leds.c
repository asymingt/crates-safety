#include "process-leds.h"

// For GPIO definitions
#include "mc1322x.h"

PROCESS(process_leds, "LEDs process");

process_event_t event_leds;

// Useful macros
#define ON  1
#define OFF 0

// Internal and external
static int gpio[LEDS_COUNT] = {LEDS_GREEN, LEDS_YELLOW, LEDS_BLUE};	

void toggle_led(int i, int on)
{
	if (i < LEDS_COUNT)
	{
		if (on)
			leds_on(gpio[i]);
		else
			leds_off(gpio[i]);
	}
}

PROCESS_THREAD(process_leds, ev, data)
{ 
	// Variable definitions
	static struct etimer timer[LEDS_COUNT];
	static int state[LEDS_COUNT] = {0, 0, 0};	
	static int delay[LEDS_COUNT] = {0, 0, 0};
	
	PROCESS_BEGIN();

	// Allocate resources to events
	event_leds = process_alloc_event();
	
	// This process runs forever
	while(1)
	{
		// Wait until a timer elapses or a button is pushed
		PROCESS_WAIT_EVENT();

		// If we have been instructed to configuere a LED
		if (ev == event_leds)
		{
		 	// Deference the data to get the configuration
		  	char conf = *((char*)data);

			// Same code for each of the three LEDS
			for (int i = 0; i < LEDS_COUNT; i++)
			{
				// If this LED is in the configuration list
				if (conf & (1 << i))
				{
					// Stop any existing timers for the LED
					etimer_stop(&timer[i]);

					// Turn the LED off
					if (conf & LEDS_OFF)
						toggle_led(i,OFF);

					// Turn the LED on
					else if (conf & LEDS_ON)
						toggle_led(i,ON);

					// FLASH
					else if (conf & LEDS_FLASH)
					{
						// Turn the LED on
						toggle_led(i,ON);

						// Single flash
						state[i] = 0;

						// Determine the flash delay
						delay[i] = (conf & LEDS_FAST ? LEDS_FLASH_FAST : LEDS_FLASH_NORM);
						
						// Set a timer
						etimer_set(&timer[i], delay[i]);
					}

					// BLINK
					else if (conf & LEDS_BLINK)
					{
						// Turn the LED on
						toggle_led(i,ON);

						// Repeating blink
						state[i] = 1;

						// Determine the flash delay
						delay[i] = (conf & LEDS_FAST ? LEDS_BLINK_FAST: LEDS_BLINK_NORM);

						// Set a timer
						etimer_set(&timer[i], delay[i]);
					}
				}
			}
		}

		// Determine the event type
		if (ev == PROCESS_EVENT_TIMER)
		{
			// Same code for each of the three LEDS
			for (int i = 0; i < LEDS_COUNT; i++)
			{
				// Was this the timer that fired?
				if (data == &timer[i])
				{
					// For one-shot timers
					if (state[i] == 0)
						toggle_led(i,OFF);

					// For the ON leg of repeating timers
					else if (state[i] == 1)
					{
						// Turn off the LED
						toggle_led(i,OFF);

						// State is now off
						state[i] = 2;

						// Set a timer
						etimer_set(&timer[i], delay[i]);
					}
					
					// For the OFF leg of repeating timers
					else
					{
						// Turn off the LED
						toggle_led(i,ON);

						// State is now off
						state[i] = 1;

						// Set a timer
						etimer_set(&timer[i], delay[i]);
					}
				}
			}
		}
	}

	PROCESS_END();
}
