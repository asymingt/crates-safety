/////////////////////////////////////////////////////////////////////////////////////////
// ASCTEC PROCESS ///////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////

#ifndef _PROCESS_ASCTEC_H_
#define _PROCESS_ASCTEC_H_

// Basic includes
#include "contiki.h"

// Packets used in asctec
#include "lib-asctec.h"

// An event that signals the arrival of a GPS string
extern process_event_t event_asctec_gps;
extern process_event_t event_asctec_sta;
extern process_event_t event_asctec_imu;

// Define the contiki process for managing the LEDs
PROCESS_NAME(process_asctec);

#endif