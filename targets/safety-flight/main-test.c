// Standard includes 
#include <stdio.h>

// Basic contiki
#include "contiki.h"

// For UART2 support
#include "contiki-uart.h"
#include "mc1322x.h"

// For networking
#include "net/rime.h"

#include "dev/button-sensor.h"

// Architecture-specific processes
#include "process-buttons.h"

// Resuable process components
#include "../process-leds.h"

// Process definitions
PROCESS(process_test, "Flight peripheral test");  	// Fundamental safety logic and interface

// Autostart these processes on contiki booting
AUTOSTART_PROCESSES(&process_test, &process_buttons, &process_leds);

// Process definition
PROCESS_THREAD(process_test, ev, data)
{
	// Thread variables
	static char leds  = 0;
	static int  power = 0;

	PROCESS_BEGIN();

	// Allocate resources to events
	event_leds = process_alloc_event();

	// Flash the LEDs
	leds = LEDS_B | LEDS_Y | LEDS_G | LEDS_BLINK | LEDS_FAST; 
	process_post(&process_leds, event_leds, &leds);

	// Main loop
	while(1)
	{
		// Wait until some event occurs
		PROCESS_WAIT_EVENT();

	    // BLACK BUTTON
	    if (ev == sensors_event && data == &button_sensor)
	    {
	    	// Debug
	      	printf("Black button pushed. Toggling power\n");

	      	// Toggle power
	      	if (power)
	      		GPIO->DATA_RESET.GPIO_09  = 1;
	      	else
	      		GPIO->DATA_SET.GPIO_09    = 1;
	      	power = !power;

	  	}
	}

	PROCESS_END();
}
