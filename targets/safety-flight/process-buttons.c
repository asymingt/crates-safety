#include "process-buttons.h"

PROCESS(process_buttons, "Buttons process");

process_event_t event_button;

// This is the main thread 
PROCESS_THREAD(process_buttons, ev, data)
{ 
  PROCESS_BEGIN();

  // Activate the button
  SENSORS_ACTIVATE(button_sensor);
  
  // Allocate memory for event handling
  event_button = process_alloc_event();
  
  // This process runs forever
  while(1)
  {
    // Wait until a timer elapses or a button is pushed
    PROCESS_WAIT_EVENT();

    // BLACK BUTTON
    if (ev == sensors_event && data == &button_sensor)
      process_post(PROCESS_BROADCAST, event_button, NULL);

  }

  PROCESS_END();
}

