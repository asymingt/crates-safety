/////////////////////////////////////////////////////////////////////////////////////////
// BUTTON PROCESS ///////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
// This process waits for a button press from the user. As soon as the button is pressed
// the safety module begins issuing a LAND command in the HEARTBEAT message. If the button 
// is pressed three times (or held down) within a five second period, a kill command is
// activated, and the power to all quadrotors is cut. This feature should only be used in
// an absolute emergency, as is places the devices at risk.
/////////////////////////////////////////////////////////////////////////////////////////

#ifndef _PROCESS_BUTTONS_H_
#define _PROCESS_BUTTONS_H_

// Basic includes
#include "contiki.h"
#include "button-sensors.h"

// An event that signals the arrival of a GPS string
extern process_event_t event_button;

// Define the contiki process for managing the LEDs
PROCESS_NAME(process_buttons);

#endif