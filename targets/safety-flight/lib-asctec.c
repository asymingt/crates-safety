#include "lib-asctec.h"

#include <stdio.h>
#include <string.h>

#define DEBUG 0

// Buffers for received data
LL_STATUS           buff_LL_STATUS;
IMU_RAWDATA         buff_IMU_RAWDATA;
IMU_CALCDATA        buff_IMU_CALCDATA;
GPS_DATA            buff_GPS_DATA;
GPS_DATA_ADVANCED   buff_GPS_DATA_ADVANCED;
RC_DATA             buff_RC_DATA;
CONTROLLER_OUTPUT   buff_CONTROLLER_OUTPUT;
CTRL_INPUT          buff_CTRL_INPUT;
WAYPOINT            buff_WAYPOINT;

// State machine
unsigned char  asctec_s = STATE_ASCTEC_WAITS1;                  // State
unsigned char  asctec_t = 0;                                    // Packet ID
unsigned short asctec_l = 0, asctec_r = 0, asctec_i = 0;        // Packet length, checksum
unsigned char  asctec_d[256];                                   // Packet 

// Feed the state machine with some data
unsigned char asctec_feed(unsigned char c)
{
    // If there's no more space in our raw data buffer
         if (asctec_s == STATE_ASCTEC_WAITS1 && c == '>') asctec_s = STATE_ASCTEC_WAITS2;   // >
    else if (asctec_s == STATE_ASCTEC_WAITS2 && c == '*') asctec_s = STATE_ASCTEC_WAITS3;   // *
    else if (asctec_s == STATE_ASCTEC_WAITS3 && c == '>') asctec_s = STATE_ASCTEC_WAITL1;   // >
    else if (asctec_s == STATE_ASCTEC_WAITL1)                                         // CHAR LENGTH1
    {
        // if (DEBUG) printf("Successfully received start sequence\n");
        asctec_l = c;
        asctec_s = STATE_ASCTEC_WAITL2;
    }
    else if (asctec_s == STATE_ASCTEC_WAITL2)                                          // CHAR LENGTH2
    {
        asctec_l = asctec_l | ((unsigned char)c << 8);
        asctec_s = STATE_ASCTEC_WAITT;
        // if (DEBUG) printf("Successfully received packet length: %d\n",asctec_l);
    }
    else if (asctec_s == STATE_ASCTEC_WAITT)   
    {
        asctec_t = c;                                                                  // TYPE
        asctec_i = 0;
        asctec_s = STATE_ASCTEC_WAITD;
        // if (DEBUG) printf("Successfully received packet type: %d\n",asctec_t);
    }
    else if (asctec_s == STATE_ASCTEC_WAITD)                                           // DATA
    {
        asctec_d[asctec_i++] = c;
        if (asctec_i == asctec_l)
            asctec_s = STATE_ASCTEC_WAITC1;
    }
    else if (asctec_s == STATE_ASCTEC_WAITC1)                                          // CHECKSUM 1
    {
        asctec_r = c; 
        asctec_s = STATE_ASCTEC_WAITC2;
    }
    else if (asctec_s == STATE_ASCTEC_WAITC2)                                          // CHECKSUM 2
    {
        asctec_r = asctec_r | ((unsigned char)c << 8);
        asctec_s = STATE_ASCTEC_WAITE1;
    } 
    else if (asctec_s == STATE_ASCTEC_WAITE1 && c == '<') asctec_s = STATE_ASCTEC_WAITE2;   // <
    else if (asctec_s == STATE_ASCTEC_WAITE2 && c == '#') asctec_s = STATE_ASCTEC_WAITE3;   // #
    else if (asctec_s == STATE_ASCTEC_WAITE3 && c == '<')                                   // <
    {
        if (DEBUG)
            printf("CRC: %x %x\n",asctec_crc16(asctec_d,asctec_l), asctec_r);

        // We've received a full packet, so eval checksum
        if (asctec_crc16(asctec_d,asctec_l) == asctec_r)
        {
            // Restart the process
            asctec_s = STATE_ASCTEC_WAITS1;

            if (DEBUG) 
                printf("Successfully received packet of type %x\n",asctec_t);

            // Copy over the data based on the packet
            switch(asctec_t)
            {
            case PD_LLSTATUS:           memcpy(&buff_LL_STATUS,         asctec_d, sizeof(buff_LL_STATUS));         return asctec_t; 
            case PD_IMURAWDATA:         memcpy(&buff_IMU_RAWDATA,       asctec_d, sizeof(buff_IMU_RAWDATA));       return asctec_t;
            case PD_IMUCALCDATA:        memcpy(&buff_IMU_CALCDATA,      asctec_d, sizeof(buff_IMU_CALCDATA));      return asctec_t;
            case PD_GPSDATA:            memcpy(&buff_GPS_DATA,          asctec_d, sizeof(buff_GPS_DATA));          return asctec_t;
            case PD_GPSDATAADVANCED:    memcpy(&buff_GPS_DATA_ADVANCED, asctec_d, sizeof(buff_GPS_DATA_ADVANCED)); return asctec_t;
            case PD_RCDATA:             memcpy(&buff_RC_DATA,           asctec_d, sizeof(buff_RC_DATA));           return asctec_t;
            case PD_CTRLOUT:            memcpy(&buff_CONTROLLER_OUTPUT, asctec_d, sizeof(buff_CONTROLLER_OUTPUT)); return asctec_t;
            case PD_CTRLINPUT:          memcpy(&buff_CTRL_INPUT,        asctec_d, sizeof(buff_CTRL_INPUT));        return asctec_t;
            case PD_WAYPOINT:           memcpy(&buff_WAYPOINT,          asctec_d, sizeof(buff_WAYPOINT));          return asctec_t;
            }
        }
        else
        {
            asctec_s = STATE_ASCTEC_WAITS1;
            printf("ID: %d CRC mismatch: %x %x\n",asctec_t,asctec_crc16(asctec_d,asctec_l), asctec_r);
        }
    }

    // Nothing yet!
    return 0;
}

unsigned short asctec_crc_update(unsigned short crc, unsigned char data)
{
    data ^= (crc & 0xff);
    data ^= data << 4;
    return ((((unsigned short )data << 8) | ((crc>>8)&0xff)) ^ (unsigned char )(data >> 4) ^ ((unsigned short )data << 3));
}
 
unsigned short asctec_crc16(void* data, unsigned short cnt)
{
    unsigned short crc=0xff;
    unsigned char * ptr=(unsigned char *) data;
    int i;
    for (i=0;i<cnt;i++)
    {
        crc = asctec_crc_update(crc,*ptr);
        ptr++;
    }
    return crc;
}