// Standard C includes
#include <stdio.h>
#include <string.h>

// GPIO for killing power
#include "mc1322x.h"

// Basic contiki
#include "contiki.h"

// Architecture-specific processes
#include "process-buttons.h"
#include "process-asctec.h"

// Shared functionality
#include "../lib-safety.h"
#include "../process-leds.h"

/////////////////////////////////////////////////////////////////////////////////////////
// PROCESS DEFINITIONS //////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////

// An event that signals the arrival of a GPS string
process_event_t event_radio_command;
process_event_t event_radio_heartbeat;
process_event_t event_radio_assoc_res;

// Process definitions
PROCESS(process_flight, "Flight safety logic");	

// Autostart these processes on contiki booting
AUTOSTART_PROCESSES(&process_flight, &process_asctec, &process_buttons, &process_leds);

/////////////////////////////////////////////////////////////////////////////////////////
// SAFETY THREAD ////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////

static char  leds;

// Called when data is received
static void broadcast_recv(struct broadcast_conn *c, const rimeaddr_t *from)
{
	// Decode the packet
	unsigned char id = safety_decode(packetbuf_dataptr(),packetbuf_datalen());

	// No point in examining empty packets, or those not destined for us
	switch (id)
	{
	case SAFETY_ID_ASSOC_RES:
		process_post(&process_flight, event_radio_assoc_res, (void*)from);
		break;
	case SAFETY_ID_HEARTBEAT:
		process_post(&process_flight, event_radio_heartbeat, (void*)from);
		break;
	case SAFETY_ID_COMMAND:
		process_post(&process_flight, event_radio_command, (void*)from);
		break;
	default:
		printf("I dont know what to do with radio message type %d\n",id);
		break;
	}

	// Flash the yelloy LED to indicate radio data arrived
	leds = LEDS_Y | LEDS_FLASH | LEDS_FAST; 
	process_post(&process_leds, event_leds, &leds);
}

static const struct broadcast_callbacks broadcast_call = {broadcast_recv};
static struct broadcast_conn broadcast;

// Process definition
PROCESS_THREAD(process_flight, ev, data)
{
 	// Protothread variables must be declared static
	static point  pos;							// 2D pos
	static float  alt, vol;						// Altitude, voltage
	static zone   zone_safe, zone_land;			// safe and land zones
	static struct etimer timer;					// Timer
	static struct etimer timeout_a;				// Timer
	static struct etimer timeout_c;				// Timer
	static unsigned char buff[128];				// Buffer

	// Start the process
	PROCESS_EXITHANDLER(broadcast_close(&broadcast);)
	PROCESS_BEGIN();

  	// Allocate process events
	event_radio_command   = process_alloc_event();
	event_radio_heartbeat = process_alloc_event();
	event_radio_assoc_res = process_alloc_event();

	// Open a broadcast connection to receive packets from UAVs
	broadcast_open(&broadcast, RADIO_CHANNEL, &broadcast_call);

/////////////////////////////////////////////////////////////////////////////////////
// IF WE REACH THIS POINT, THE SYSTEM HAS SHIFTED TO AN INIT STATE //////////////////
/////////////////////////////////////////////////////////////////////////////////////
// The flight module will try to associate with the ground station                 //

init: 
	printf("Transitioning to INIT state\n");
  	
	// Set the RIME address
	rimeaddr_copy(&safety_assoc_req.addr,&rimeaddr_node_addr);

	// Send out association requests at 1Hz
	etimer_set(&timer, CLOCK_SECOND);

	// Main loop
	while(1)
	{
		// Wait until some event occurs
		PROCESS_WAIT_EVENT();

		// Timer expires and heartbeat message needs to be received
		if (ev == PROCESS_EVENT_TIMER && data == &timer)
		{
			// Reset the timer
			etimer_reset(&timer);

			// Send the request
			printf("Sending association request\n");
			packetbuf_copyfrom(buff,safety_encode(buff,&rimeaddr_ground,SAFETY_ID_ASSOC_REQ));
			broadcast_send(&broadcast);
		}

		// A new command arrives from the base station
		if (ev == event_radio_assoc_res)
		{
			printf("Association response received\n");

			// Set the RIME address to that given byt he
			rimeaddr_set_node_addr(&safety_assoc_res.addr);

			// Now, jump to safe mode
			goto safe;
		}
	}

/////////////////////////////////////////////////////////////////////////////////////
// IF WE REACH THIS POINT, THE SYSTEM HAS SHIFTED TO AN SAFE STATE //////////////////
/////////////////////////////////////////////////////////////////////////////////////

safe:
	printf("Transitioning to SAFE state\n");
	leds = LEDS_B | LEDS_ON; 
	process_post(&process_leds, event_leds, &leds);

	// Send out association requests at 1Hz
	etimer_set(&timer,      CLOCK_SECOND);
	//etimer_set(&timeout_a,	CLOCK_SECOND*5);
	//etimer_set(&timeout_c,  CLOCK_SECOND*5);

	// Main loop
	while(1)
	{
		// Wait until some event occurs
		PROCESS_WAIT_EVENT();

		// Timer expires and heartbeat message needs to be received
		/*
		if (ev == PROCESS_EVENT_TIMER && data == &timeout_a)
		{
			printf("ASCTEC timeout\n");
			goto land;
		}
		if (ev == PROCESS_EVENT_TIMER && data == &timeout_c)
		{
			printf("COMMAND timeout\n");
			goto land;
		}
		*/
		if (ev == PROCESS_EVENT_TIMER && data == &timer)
		{
			// Reset the timer
			etimer_reset(&timer);

			// Send the request
			packetbuf_copyfrom(buff,safety_encode(buff,&rimeaddr_broadcast,SAFETY_ID_HEARTBEAT));
			broadcast_send(&broadcast);
		}

		// A new command arrives from the base station
		if (ev == event_asctec_sta)
		{
			// Reset the command timeout
			etimer_reset(&timeout_a);

			// Flash the green LED to indicate FCS data received
			leds = LEDS_G | LEDS_BLINK | LEDS_FAST; 
			process_post(&process_leds, event_leds, &leds);

			// Extract data
			LL_STATUS* ptr = (LL_STATUS*) data;
			safety_heartbeat.voltage = ((float)ptr->battery_voltage_1) / 1e3;
			printf("Current voltage: "); printfloat(vol); printf("\n");
			
			// Are we outside the safe zone?
			/*
			if (vol > zone_safe.voltage)
			{
				if (vol > zone_land.voltage)
					goto kill;
				else
					goto land;
			}
			*/
		}

		// A new command arrives from the base station
		if (ev == event_asctec_imu)
		{
			// Reset the command timeout
			etimer_reset(&timeout_a);

			// Flash the green LED to indicate FCS data received
			leds = LEDS_G | LEDS_BLINK | LEDS_FAST; 
			process_post(&process_leds, event_leds, &leds);

			// Extract data
			IMU_CALCDATA* ptr = (IMU_CALCDATA*) data;
			safety_heartbeat.position.z = ((float)ptr->height)  / 1e3;
			safety_heartbeat.velocity.z = ((float)ptr->dheight) / 1e3;
			printf("Current altitude: "); 
			printfloat(safety_heartbeat.position.z); 
			printf("\n");
			printf("Current vertical velocity: "); 
			printfloat(safety_heartbeat.velocity.z); 
			printf("\n");

			// Are we outside the safe zone?
			/*
			if (alt > zone_safe.altitude)
			{
				if (alt > zone_land.altitude)
					goto kill;
				else
					goto land;
			}
			*/

		}

		// A new command arrives from the base station
		if (ev == event_asctec_gps)
		{
			// Reset the command timeout
			etimer_reset(&timeout_a);

			// Flash the green LED to indicate FCS data received
			leds = LEDS_G | LEDS_BLINK | LEDS_FAST; 
			process_post(&process_leds, event_leds, &leds);

			// Extract the position
			GPS_DATA_ADVANCED* ptr = (GPS_DATA_ADVANCED*) data;
			safety_heartbeat.position.x = ((float)ptr->longitude) / 1e7;
			safety_heartbeat.position.y = ((float)ptr->latitude)  / 1e7;
			safety_heartbeat.velocity.x = ((float)ptr->speed_x)   / 1e3;
			safety_heartbeat.velocity.y = ((float)ptr->speed_y)   / 1e3;
			printf("Current position: "); 
			printfloat(safety_heartbeat.position.x);
			printf(", "); 
			printfloat(safety_heartbeat.position.y); 
			printf("\n");
			printf("Current velocity: "); 
			printfloat(safety_heartbeat.velocity.x);
			printf(", "); 
			printfloat(safety_heartbeat.velocity.y); 
			printf("\n");

			// Are we outside the safe zone?
			/*
			if (!is_point_in_polygon(zone_safe.n, zone_safe.vertx, zone_safe.verty, &pos))
			{
				// If we are outside the land zone
				if (!is_point_in_polygon(zone_land.n, zone_land.vertx, zone_land.verty, &pos))
					goto kill;
				else
					goto land;
			}
			*/
		}

		// A new command arrives from the ground station
		if (ev == event_radio_command)
		{
			etimer_reset(&timeout_c);
			for (int i = 0; i < safety_command.num; i++)
			{
				if (rimeaddr_cmp(&rimeaddr_node_addr, &safety_command.address[i]))
				{
					if (safety_command.command[i] == UAV_STATE_STOP) goto stop;
					if (safety_command.command[i] == UAV_STATE_LAND) goto land;
					if (safety_command.command[i] == UAV_STATE_KILL) goto kill;
				}
			}
		}
	}

/////////////////////////////////////////////////////////////////////////////////////
// IF WE REACH THIS POINT, THE SYSTEM HAS SHIFTED TO A STOP STATE //////////////////
/////////////////////////////////////////////////////////////////////////////////////

stop:
	printf("Transitioning to IDLE state\n");
	leds = LEDS_B | LEDS_BLINK | LEDS_FAST; 
	process_post(&process_leds, event_leds, &leds);

	// Main loop
	while(1)
	{
		// A new command arrives from the ground station
		if (ev == event_radio_command)
		{
			etimer_reset(&timeout_c);
			for (int i = 0; i < safety_command.num; i++)
			{
				if (rimeaddr_cmp(&rimeaddr_node_addr, &safety_command.address[i]))
				{
					if (safety_command.command[i] == UAV_STATE_SAFE) goto safe;
					if (safety_command.command[i] == UAV_STATE_LAND) goto land;
					if (safety_command.command[i] == UAV_STATE_KILL) goto kill;
				}
			}
		}

		// Wait until some event occurs
		PROCESS_WAIT_EVENT();
	}

/////////////////////////////////////////////////////////////////////////////////////
// IF WE REACH THIS POINT, THE SYSTEM HAS SHIFTED TO AN LAND STATE //////////////////
/////////////////////////////////////////////////////////////////////////////////////

land:
	printf("Transitioning to LAND state\n");
	leds = LEDS_B | LEDS_BLINK; 
	process_post(&process_leds, event_leds, &leds);

	// Main loop
	while(1)
	{
		// Wait until some event occurs
		PROCESS_WAIT_EVENT();
	}

/////////////////////////////////////////////////////////////////////////////////////
// IF WE REACH THIS POINT, THE SYSTEM HAS SHIFTED TO AN KILL STATE //////////////////
/////////////////////////////////////////////////////////////////////////////////////

kill:
	printf("Transitioning to KILL state\n");
	leds = LEDS_B | LEDS_OFF; 
	process_post(&process_leds, event_leds, &leds);

	// Main loop
	while(1)
	{
	  	// Kill power to the system
	  	GPIO->DATA_SET.GPIO_09 = 1;

		// Wait until some event occurs
		PROCESS_WAIT_EVENT();
	}

	// Main loop
	PROCESS_END();

}
