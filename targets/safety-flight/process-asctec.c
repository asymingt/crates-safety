#include "process-asctec.h"

// Standard includes
#include <stdio.h>

// For UART support
#include "contiki-uart.h"
#include "mc1322x.h"

// An event that signals the arrival of a GPS string
process_event_t event_asctec_gps;
process_event_t event_asctec_sta;
process_event_t event_asctec_imu;

PROCESS(process_asctec, "Asctec process");

// Process definition
PROCESS_THREAD(process_asctec, ev, data)
{
	static struct etimer et_asctec_tx;
	static struct etimer et_asctec_rx;
	uint8_t type;

	PROCESS_BEGIN();

  	// Allocate to processes
  	event_asctec_gps = process_alloc_event();
  	event_asctec_sta = process_alloc_event();
  	event_asctec_imu = process_alloc_event();

	// Set UART2 to 57.6K @ 8N1
	uart2_init(383, 9999, UCON_SAMP_8X);
	
	// Set the commands to fire out at 10Hz
	etimer_set(&et_asctec_tx, CLOCK_SECOND / 5);	// Poll  at 5Hz
	etimer_set(&et_asctec_rx, CLOCK_SECOND / 50);	// Check at 50Hz

	// Get some RAW data from the GPS
	while(1)
	{
		// When the timer has elapsed
		PROCESS_WAIT_EVENT();

		// When the timer expires we need to poll for some data
		if (ev == PROCESS_EVENT_TIMER && data == &et_asctec_tx)
		{
			// Reset the timer
			etimer_reset(&et_asctec_tx);

			// This is a special sequence of characters that will poll
			// the LLP for LL_STATUS and GPS_DATA packets, which we'll
			// then use to check battery / position
			uart2_putc('>'); // >
			uart2_putc('*'); // *
			uart2_putc('>'); // >
			uart2_putc('p'); // p
			uart2_putc(0x05); // 0x0001 | 0x0004
			uart2_putc(0x02); // 0x0200
		}

		// When the timer expires we need to poll for some data
		if (ev == PROCESS_EVENT_TIMER && data == &et_asctec_rx)
		{
			// Reset the timer
			etimer_reset(&et_asctec_rx);

			// Keep doing this until there is no data left
			while (uart2_can_get())
			{
				// Feed a character into the parsing engine and 
				// return if a complete packet is received
				type = asctec_feed(uart2_getc());

				// Post advanced GPS data
				if (type==PD_GPSDATAADVANCED)	
					process_post(PROCESS_BROADCAST, event_asctec_gps, &buff_GPS_DATA_ADVANCED);
				
				// Post the platform status
				if (type==PD_LLSTATUS)		
					process_post(PROCESS_BROADCAST, event_asctec_sta, &buff_LL_STATUS);
			
				// Post the platform status
				if (type==PD_IMUCALCDATA)		
					process_post(PROCESS_BROADCAST, event_asctec_imu, &buff_IMU_CALCDATA);
			}
		}
	}

	PROCESS_END();
}