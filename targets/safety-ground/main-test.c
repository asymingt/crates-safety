// Standard includes 
#include <stdio.h>

// Basic contiki
#include "contiki.h"

// For UART2 support
#include "contiki-uart.h"
#include "mc1322x.h"

// For networking
#include "net/rime.h"
#include "custom-buttons.h"

// Architecture-specific processes
#include "process-buttons.h"

// Resuable process components
#include "../lib-safety.h"
#include "../process-leds.h"

// Process definitions
PROCESS(process_test, "Ground peripheral test");  	// Fundamental safety logic and interface

// Autostart these processes on contiki booting
AUTOSTART_PROCESSES(&process_test, &process_buttons, &process_leds);

// Process definition
PROCESS_THREAD(process_test, ev, data)
{
	// Thread variables
	static char leds;						// LED configuration

	PROCESS_BEGIN();
	
	// Main loop
	while(1)
	{
		// Wait until some event occurs
		PROCESS_WAIT_EVENT();

	    // BLACK BUTTON
	    if (ev == sensors_event && data == &button_sensor_black)
	      printf("Black button pushed\n");

	    // GREEN BUTTON
	    if (ev == sensors_event && data == &button_sensor_green)
	      printf("Green button pushed\n");

	    // RED BUTTON
	    if (ev == sensors_event && data == &button_sensor_red)
	      printf("Red button pushed\n");

	// Flash the LEDs
	leds = LEDS_G | LEDS_BLINK | LEDS_FAST; 
	process_post(&process_leds, event_leds, &leds);
	
	}

	PROCESS_END();
}
