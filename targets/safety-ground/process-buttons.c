#include "process-buttons.h"

#include <stdio.h>

PROCESS(process_buttons, "Buttons process");

process_event_t event_button_black;
process_event_t event_button_green;
process_event_t event_button_red;

// This is the main thread 
PROCESS_THREAD(process_buttons, ev, data)
{ 
  // Variable definitions
  static char state;                        // Character state
  static struct etimer et_deb_r, et_res_r;  // Trigger hold state
  static struct etimer et_deb_g, et_res_g;  // Trigger hold state
  static struct etimer et_deb_b, et_res_b;  // Trigger hold state
  static int et_push_b = 0;                 // Number of pushes
  static int et_push_g = 0;                 // Number of pushes
  static int et_push_r = 0;                 // Number of pushes
  
  PROCESS_BEGIN();

  // Activate the button
  SENSORS_ACTIVATE(button_sensor_black);
  SENSORS_ACTIVATE(button_sensor_green);
  SENSORS_ACTIVATE(button_sensor_red);

  // Allocate memory for event handling
  event_button_black = process_alloc_event();
  event_button_green = process_alloc_event();
  event_button_red   = process_alloc_event();

  etimer_set(&et_deb_r, CLOCK_SECOND);
  etimer_set(&et_deb_g, CLOCK_SECOND);
  etimer_set(&et_deb_b, CLOCK_SECOND);

  // This process runs forever
  while(1)
  {
    // Wait until a timer elapses or a button is pushed
    PROCESS_WAIT_EVENT();

    // When the command timer elapses, broadcast commands to all platforms
    if (ev == PROCESS_EVENT_TIMER && data == &et_res_b) et_push_b = 0;
    if (ev == sensors_event && data == &button_sensor_black)
    {
      if (etimer_expired(&et_deb_b))
      {
        et_push_b++;
        etimer_set(&et_deb_b, CLOCK_SECOND / 5);
        etimer_set(&et_res_b, CLOCK_SECOND);
        process_post(PROCESS_BROADCAST, event_button_black, &et_push_b);
      }
    }

    // GREEN BUTTON
    if (ev == PROCESS_EVENT_TIMER && data == &et_res_g) et_push_g = 0;
    if (ev == sensors_event && data == &button_sensor_green)
    {
      if (etimer_expired(&et_deb_g))
      {
        et_push_g++;
        etimer_set(&et_deb_g, CLOCK_SECOND / 5);
        etimer_set(&et_res_g, CLOCK_SECOND);
        process_post(PROCESS_BROADCAST, event_button_green, &et_push_g);
      }
    }

    // RED BUTTON
    if (ev == PROCESS_EVENT_TIMER && data == &et_res_r) et_push_r = 0;
    if (ev == sensors_event && data == &button_sensor_red)
    {
      if (etimer_expired(&et_deb_r))
      {
        printf("got here\n");
        et_push_r++;
        etimer_set(&et_deb_r, CLOCK_SECOND / 5);
        etimer_set(&et_res_r, CLOCK_SECOND);
        process_post(PROCESS_BROADCAST, event_button_red, &et_push_r);
      }
    }
  }

  PROCESS_END();
}