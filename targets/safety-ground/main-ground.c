// Basic contiki
#include "contiki.h"

// For UART2 support
#include "contiki-uart.h"
#include "mc1322x.h"

// For networking
#include "net/rime.h"

// Experimental configuration 
#include "../lib-safety.h"
#include "../process-leds.h"

// Platform-specific button
#include "process-buttons.h"

/////////////////////////////////////////////////////////////////////////////////////////
// PROCESS DEFINITIONS //////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////

// An event that signals the arrival of a GPS string
process_event_t event_radio_heartbeat;
process_event_t event_radio_assoc_req;

// Process definitions
PROCESS(process_ground,"Core safety logic"); 

// Autostart these processes on contiki booting
AUTOSTART_PROCESSES(&process_ground, &process_buttons, &process_leds);

/////////////////////////////////////////////////////////////////////////////////////////
// SAFETY THREAD ////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////

// LED configuration
static char leds;

// Called when data is received
static void broadcast_recv(struct broadcast_conn *c, const rimeaddr_t *from)
{
	// Decode the packet
	unsigned char id = safety_decode(packetbuf_dataptr(),packetbuf_datalen());

	// No point in examining empty packets, or those not destined for us
	switch (id)
	{
	case SAFETY_ID_ASSOC_REQ:
		process_post(&process_ground, event_radio_assoc_req, (void*)from);
		break;
	case SAFETY_ID_HEARTBEAT:
		process_post(&process_ground, event_radio_heartbeat, (void*)from);
		break;
	default:
		printf("I dont know what to do with radio message type %d\n",id);
		break;
	}

	// Flash the yelloy LED to indicate radio data arrived
	leds = LEDS_Y | LEDS_FLASH | LEDS_FAST; 
	process_post(&process_leds, event_leds, &leds);

}
static const struct broadcast_callbacks broadcast_call = {broadcast_recv};
static struct broadcast_conn broadcast;

// Process definition
PROCESS_THREAD(process_ground, ev, data)
{
	// Thread variables
	static struct etimer timer;				// Timer
	static unsigned char buff[128];			// Buffer

	// Begin process
	PROCESS_EXITHANDLER(broadcast_close(&broadcast);)
	PROCESS_BEGIN();

  	// Allocate process events
	event_radio_heartbeat = process_alloc_event();
	event_radio_assoc_req = process_alloc_event();

	// Set the RIME address to indicate its the base station, and open radio channel
	rimeaddr_set_node_addr(&rimeaddr_ground);
	broadcast_open(&broadcast, RADIO_CHANNEL, &broadcast_call);

	// Set the commands to fire out at a fixed rate,
	etimer_set(&timer, CLOCK_SECOND);

	// Main loop
	while(1)
	{
		// Wait until some event occurs
		PROCESS_WAIT_EVENT();

		// When the command timer elapses, broadcast commands to all platforms
		if (ev == PROCESS_EVENT_TIMER && data == &timer)
		{
			// Reset the timer
			etimer_reset(&timer);

			// Only broadcast commands if some platform actually exists
			if (safety_command.num > 0)
			{
				printf("Broadcasting commands to %d platforms\n",safety_command.num);
				packetbuf_copyfrom(buff,safety_encode(buff,&rimeaddr_broadcast,SAFETY_ID_COMMAND));
				broadcast_send(&broadcast);
			}
		}

	    // BLACK BUTTON
	    else if (ev == event_button_black)
	    {
	      	printf("Black button pushed %d times\n", *((int*)data));
	    }

	    // GREEN BUTTON
	    else if (ev == event_button_green)
	    {
			if (*((int*)data) > 3)
			{
				printf("Green button pushed %d times\n. Landing platforms.", *((int*)data));
	    		for (int i = 0; i < safety_command.num; i++)
	    			safety_command.command[i] = UAV_STATE_LAND;
	    	}
	    	else
	    	{
	    		printf("Green button pushed %d times\n. Starting platforms.", *((int*)data));
	    		for (int i = 0; i < safety_command.num; i++)
	    			safety_command.command[i] = UAV_STATE_SAFE;
	    	}
	    }  

	    // RED BUTTON
	    else if (ev == event_button_red)
	    {
	    	// If we push the red button more than three times in 5 seconds, power is cut
	    	if (*((int*)data) > 3)
	    	{
	    		printf("Red button pushed %d times\n. Killing platforms.", *((int*)data));
	    		for (int i = 0; i < safety_command.num; i++)
	    			safety_command.command[i] = UAV_STATE_KILL;
	    	}
	    	else
	    	{
	    		printf("Green button pushed %d times\n. Starting platforms.", *((int*)data));
	    		for (int i = 0; i < safety_command.num; i++)
	    			safety_command.command[i] = UAV_STATE_STOP;
	    	}
	    }

	    // NETWORK TRANSER PROTOCOL /////////////////////////////////////////////////////

		// When an association request arrives
		else if (ev == event_radio_assoc_req)
		{
			printf("Association request received\n");

			// Check first to see if there are any outstanding requests
			int idx = -1;
			for (int i = 0; i < safety_command.num; i++)
				if (safety_command.command[i] == UAV_STATE_INIT)
					idx = i;
	
			// If no match was made, we can assume this is a new device
			if (idx < 0)
			{
				if (safety_command.num < MAX_PLATFORMS)
				{
					rimeaddr_copy(&safety_command.address[safety_command.num],&rimeaddr_null);
					safety_command.address[safety_command.num].u8[0] = safety_command.num + 1;
					safety_command.command[safety_command.num] = UAV_STATE_INIT;
					idx = safety_command.num++;
				}
				else
				{
					printf("Too many platforms already associated\n");
					continue;
				}
			}
			
			// Package up an association response to the platform, with the new address
			rimeaddr_copy(&safety_assoc_res.addr, &safety_command.address[idx]);
			packetbuf_copyfrom(buff,safety_encode(buff,&safety_assoc_req.addr,SAFETY_ID_ASSOC_RES));
			broadcast_send(&broadcast);
			printf("Association response sent\n");
		}

		// When a heartbeat signal arrives
		else if (ev == event_radio_heartbeat)
		{
			// Complete any outstanding associations with new platforms
			for (int i = 0; i < safety_command.num; i++)
			{
				if (safety_command.command[i] == UAV_STATE_INIT)
				{
					if (rimeaddr_cmp(data, &safety_command.address[i]))
					{
						printf("Heartbeat received from platform %d, switching from IDLE -> SAFE \n",i+1);
						safety_command.command[i] = UAV_STATE_SAFE;
					}
				}
			}

			// Debug output
			printf("Heartbeat received from UAV with [POS] ");
			printfloat(safety_heartbeat.position.x); 
			printf(",");
			printfloat(safety_heartbeat.position.y); 
			printf(",");
			printfloat(safety_heartbeat.position.z); 
			printf(" [VEL] ");
			printfloat(safety_heartbeat.velocity.x); 
			printf(",");
			printfloat(safety_heartbeat.velocity.y); 
			printf(",");
			printfloat(safety_heartbeat.velocity.z);
			printf(" [BAT] ");
			printfloat(safety_heartbeat.voltage);
			printf("\n");
		}
	}

	PROCESS_END();
}