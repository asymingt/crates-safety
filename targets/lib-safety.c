#include "lib-safety.h"

#define DEBUG 0

// BASIC GEOMETRY ////////////////////////////////////////////////////////////////////////

// Useful declarations
#define min(x, y) ((x)<(y)?(x):(y))
#define max(x, y) ((x)>(y)?(x):(y))

// From http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html
int is_point_in_polygon(int n, float *vertx, float *verty, point *p)
{
  int i, j; int c = 0;
  for (i = 0, j = n - 1; i < n; j = i++)
  {
    if (((verty[i]>p->y) != (verty[j]>p->y)) 
        && (p->x < (vertx[j]-vertx[i]) * (p->y-verty[i]) / (verty[j]-verty[i]) + vertx[i]) )
       c = !c;
  }
  return c;
}
 
// Find the orientation of ordered triplet(p, q, r).
// 0 - collinear
// 1 - clockwise
// 2 - counterclockwise
int orientation(point *p, point *q, point *r)
{
    int val = (q->y - p->y) * (r->x - q->x) - (q->x - p->x) * (r->y - q->y);
 
    if (0 == val)
        return 0;

    return val > 0 ? 1: 2;
}
 
// Given three colinear points p, q, r
// check if point q lines on line segment(p, r)
// 0 - Not on the segment
// 1 - On the segment
int is_on_segment(point *p, point *q, point *r)
{
    if (q->x <= max(p->x, r->x) && q->x >= min(p->x, r->x) &&
        q->y <= max(p->y, r->y) && q->y >= min(p->y, r->y))
        return 1;
 
    return 0;
}
 
// Exam if the line segment (p1, q1) and (p2, q2) are intersected
// 0 - Not intersected
// 1 - Intersected
int is_line_intersected(point *p1, point *q1, point *p2, point *q2)
{
    int o1 = orientation(p1, q1, p2);
    int o2 = orientation(p1, q1, q2);
    int o3 = orientation(p2, q2, p1);
    int o4 = orientation(p2, q2, q1);
    
    if (o1 != o2 && o3 != o4) 
        return 1;
 
    if (o1 == 0 && is_on_segment(p2, p2, q1))
        return 1;
    if (o2 == 0 && is_on_segment(p1, q2, q1))
        return 1;
    if (o3 == 0 && is_on_segment(p2, p1, q2))
        return 1;
    if (o4 == 0 && is_on_segment(p2, q1, q2))
        return 1;
 
    return 0;
}
 
// Exam if a point is in a rectangle
// 0 - Out
// 1 - In
int is_point_in_rectangle(point *p, rectangle *rec) 
{
    point *rp = rec->p;

    int o = orientation(rp + 0, rp + 1, p);
 
    if (o == 0)
        return 1;
 
    return (o == orientation(rp + 1, rp + 2, p)
        &&  o == orientation(rp + 2, rp + 3, p)
        &&  o == orientation(rp + 3, rp + 0, p)); 
}
 
// From: http://blog.theliuy.com/determine-if-two-rotated-rectangles-overlap-each-other/
int do_rectangles_overlap(rectangle *rec0, rectangle *rec1)
{
    point *p0 = rec0->p;
    point *p1 = rec1->p;
    int i, j;
    point c0, c1;
 
    // check sides of each rectangle
    // if two are intersected, the rectangle should intersected
    for (i = 0; i < 4; ++i)
        for (j = 0; j, j < 4; ++j)
            if (is_line_intersected(p0 + i, p0 + (i + 1) % 4, p1 + j, p1 + (j + 1) % 4))
                return 1;
 
    // check if one point of rectangle 0 is in rectangle 1
    // or one point of rectangle 1 is in rectangle 0
    if (is_point_in_rectangle(p1, rec0) || is_point_in_rectangle(p0, rec1))
        return 1;
 
    return 0;
}

// BASIC PACKETIZATION /////////////////////////////////////////////////////////////////

// Special addresses
rimeaddr_t rimeaddr_broadcast = { { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 } };
rimeaddr_t rimeaddr_ground    = { { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF } };

// External packets
SAFETY_ASSOC_REQ  safety_assoc_req;
SAFETY_ASSOC_RES  safety_assoc_res;
SAFETY_HEARTBEAT  safety_heartbeat;
SAFETY_COMMAND    safety_command;

unsigned short safety_crc_update(unsigned short crc, unsigned char data)
{
    data ^= (crc & 0xff);
    data ^= data << 4;
    return ((((unsigned short )data << 8) | ((crc>>8)&0xff)) ^ (unsigned char )(data >> 4) ^ ((unsigned short )data << 3));
}
 
unsigned short safety_crc16(void* data, unsigned short cnt)
{
    unsigned short crc=0xff;
    unsigned char * ptr=(unsigned char *) data;
    int i;
    for (i=0;i<cnt;i++)
    {
        crc = safety_crc_update(crc,*ptr);
        ptr++;
    }
    return crc;
}

// Encode a packet to  the wire
unsigned short safety_encode(void *buff, rimeaddr_t *dest, unsigned char id)
{
    // Packet length
    unsigned short len = 0, crc;

    // 0-7 : Copy over the destination address
    rimeaddr_copy(buff,dest);
    
    // 8 : Set the ID
    ((unsigned char*)buff)[RIMEADDR_SIZE] = id;

    // 9 - (LEN+9) : Copy over the data of lenth LEN
    switch(id)
    {
    case SAFETY_ID_ASSOC_REQ: 
        len = sizeof(SAFETY_ASSOC_REQ);
        memcpy(&((unsigned char*)buff)[RIMEADDR_SIZE+1], &safety_assoc_req, len); 
        break;
    case SAFETY_ID_ASSOC_RES: 
        len = sizeof(SAFETY_ASSOC_RES);
        memcpy(&((unsigned char*)buff)[RIMEADDR_SIZE+1], &safety_assoc_res, len);
        break;
    case SAFETY_ID_HEARTBEAT: 
        len = sizeof(SAFETY_HEARTBEAT);
        memcpy(&((unsigned char*)buff)[RIMEADDR_SIZE+1], &safety_heartbeat, len); 
        break;
    case SAFETY_ID_COMMAND: 
        len = sizeof(SAFETY_COMMAND);
        memcpy(&((unsigned char*)buff)[RIMEADDR_SIZE+1], &safety_command, len);   
        break;
    default:
        return len;
    }
    
    // (LEN+9) - (LEN+11) : Generate the CRC
    crc = safety_crc16(buff+RIMEADDR_SIZE+1,len);
    memcpy(buff+RIMEADDR_SIZE+1+len,&crc,2);

    if (DEBUG)
    {
        printf("CRC: %x\n",safety_crc16(buff+RIMEADDR_SIZE+1,len));
        printf("Message: ");
        for (int j = 0; j < RIMEADDR_SIZE+len+3; j++)  
            printf("%x ",((unsigned char*)buff)[j]);
        printf("\n");
    }

    // Return the packet length
    return RIMEADDR_SIZE+len+3;
}

// Decode a packet from the wire
unsigned char safety_decode(void *buff, int n)
{
    // State machine
    unsigned char  safety_s = STATE_SAFETY_WAITA, c;            // State
    unsigned char  safety_t = 0;                                // Packet ID
    unsigned short safety_l = 0, safety_r = 0, safety_i = 0;    // Packet length, checksum
    unsigned char  safety_d[128];    
    rimeaddr_t     safety_a;

    printf("Message: ");
    for (int j = 0; j < n; j++)  
        printf("%x ",((unsigned char*)buff)[j]);
    printf("\n");

    // Keep feeding the safety state engine, and returns packet ID if success
    for (int i = 0; i < n; i++)
    {
        // Grab a character
        c = ((unsigned char*)buff)[i];

        // If there's no more space in our raw data buffer
        if (safety_s == STATE_SAFETY_WAITA)
        {
            // Copy over the address
            safety_a.u8[safety_i++] = c;

            // If we have received the entire address, switch state
            if (safety_i == RIMEADDR_SIZE)
                safety_s = STATE_SAFETY_WAITT;
        }
        else if (safety_s == STATE_SAFETY_WAITT)   
        {
            if (DEBUG) 
            {
                printf("Address: ");
                for (int j = 0; j < RIMEADDR_SIZE; j++)  
                    printf("%x ",safety_a.u8[j]);
                printf("\n");
            }

            // Initialize message type and
            safety_t = c;                                                                  // TYPE

            // 9 - (LEN+9) : Copy over the data of lenth LEN
                 if (safety_t == SAFETY_ID_ASSOC_REQ)   safety_l = sizeof(SAFETY_ASSOC_REQ);
            else if (safety_t == SAFETY_ID_ASSOC_RES)   safety_l = sizeof(SAFETY_ASSOC_RES);
            else if (safety_t == SAFETY_ID_HEARTBEAT)   safety_l = sizeof(SAFETY_HEARTBEAT);
            else if (safety_t == SAFETY_ID_COMMAND)     safety_l = sizeof(SAFETY_COMMAND);  
            else return 0x0;
           
            if (DEBUG) 
                printf("Type: %x",safety_t);

            // Switch to data
            safety_s = STATE_SAFETY_WAITD;
            safety_i = 0;                  

        }
        else if (safety_s == STATE_SAFETY_WAITD)                                           // DATA
        {
            safety_d[safety_i++] = c;
            if (safety_i == safety_l)
                safety_s = STATE_SAFETY_WAITC1;
        }
        else if (safety_s == STATE_SAFETY_WAITC1)                                           // CHECKSUM 1
        {
            if (DEBUG) 
            {
                printf("Data: ");
                for (int j = 0; j < safety_l; j++)  
                    printf("%x ",safety_d[j]);
                printf("\n");
            }

            safety_r = c; 
            safety_s = STATE_SAFETY_WAITC2;
        }
        else if (safety_s == STATE_SAFETY_WAITC2)                                          // CHECKSUM 2
        {
            // Finish CRC
            safety_r = safety_r | ((unsigned char)c << 8);

            if (DEBUG)
                printf("CRC: %x %x\n",safety_crc16(safety_d,safety_l),safety_r);

            // We've received a full packet, so eval checksum
            if (safety_crc16(safety_d,safety_l) == safety_r)
            {
                if (DEBUG) 
                    printf("Successfully received packet of type %x\n",safety_t);

                // Check if this is  broadcast (null) or unicast to us
                if (rimeaddr_cmp(&safety_a,&rimeaddr_node_addr) || rimeaddr_cmp(&safety_a,&rimeaddr_broadcast))
                {
                    // 9 - (LEN+9) : Copy over the data of lenth LEN
                    switch(safety_t)
                    {
                    case SAFETY_ID_ASSOC_REQ:   memcpy(&safety_assoc_req, safety_d, sizeof(SAFETY_ASSOC_REQ));   return safety_t; 
                    case SAFETY_ID_ASSOC_RES:   memcpy(&safety_assoc_res, safety_d, sizeof(SAFETY_ASSOC_RES));   return safety_t; 
                    case SAFETY_ID_HEARTBEAT:   memcpy(&safety_heartbeat, safety_d, sizeof(SAFETY_HEARTBEAT));   return safety_t; 
                    case SAFETY_ID_COMMAND:     memcpy(&safety_command,   safety_d, sizeof(SAFETY_COMMAND));     return safety_t; 
                    default:
                        return 0;
                    }
                }
                else
                {
                    if (DEBUG) 
                        printf("Packet destination address problematic\n");
                    return 0x0;
                }
            }
            else
            {
              if (DEBUG) 
                    printf("CRC mismatch\n");
                return 0x0;
            }
        }
    }

    // We should never get here
    return 0x0;
}

// GENERAL FUNCTIONS ////////////////////////////////////////////////////////////////////

void printfloat(float a)
{
	unsigned long x = *(unsigned long*)&a;
	unsigned int sign = x >> 31;
	unsigned int exp = ((x >> 23) & 0xff) - 127;
	unsigned int man = x & ((1 << 23) - 1);
	man |= 1 << 23;
	if (sign) printf("-");
	printf("%d", man >> (23 - exp));
	printf(".");
	unsigned int frac = man & ((1 << (23-exp)) - 1);
	unsigned int base = 1 << (23 - exp);
	int c = 0;
	while (frac != 0 && c++ < 6)
	{
		frac *= 10;
		printf("%d", (frac / base));
		frac %= base;
	}
}