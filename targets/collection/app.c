#include "contiki.h"
#include "lib/sensors.h"
#include "dev/leds.h"
#include "ff.h"
#include "cfs/cfs.h"
#include <stdio.h>
#include <string.h>
#include "nv08c.h"
#include "net/rime.h"
#include "maca.h"
#define WAIT(e,t)   etimer_set(e, t);\
        PROCESS_WAIT_EVENT_UNTIL(etimer_expired(e));

#define FLASH_LED(l) {leds_on(l); clock_delay_msec(50); leds_off(l); clock_delay_msec(50);}
#define POLLING_HZ 10
#define DURATION (60*60*6)
#define BEE_CHANNEL 135
#define GPS_CHANNEL 136
#define MAX_PACKETLEN 128
PROCESS(gps_proc, "GPS collection");
PROCESS(bee_proc, "ZigBee collection");

AUTOSTART_PROCESSES(&gps_proc, &bee_proc);

static const struct nv08c_command time_command[] = {
#define APP_CMD_ON_TIME 0
  { 2, { 0x1f, 0x01 } },

#define APP_CMD_OFF_TIME 1
  { 2, { 0x1f, 0x00 } },
}; 

int
message_is_time(char * bc)  {
  return (!nv08c_sensor.value(NV08C_VALUE_BUSY) &&
      bc[0] == NV08C_BINR_DLE && bc[1] == 0x72);
}

void
gc_packet_received(struct abc_conn *gc) {

}

void
gc_packet_sent(struct abc_conn *gc, int status, int num_tx) {

}

static struct abc_callbacks gc_callbacks = { gc_packet_received, gc_packet_sent };
static struct abc_conn gc;
PROCESS_THREAD(gps_proc, ev, data)
{
  PROCESS_EXITHANDLER(abc_close(&gc);); 
  PROCESS_BEGIN();
  static struct etimer et;
  static int t = 0, raw_file = -1, sync_file = -1;
  static char *binr_chars, drv = 0, ts[20];
  static int i, length, time;
  static FATFS fs;
  static unsigned long cs;
  static clock_time_t ct; 
  //Mount the SD Card 
  printf("f_mount:%d\n",f_mount(drv,&fs));
  
  //Initialise UART connection to GPS sensor 
  SENSORS_ACTIVATE(nv08c_sensor);
  
  //Enable UART Loopback 
  nv08c_sensor.configure(NV08C_CONFIG_PIPE_TO_SERIAL, 1); 
  
  //Send start-up commands to the GPS sensor 
  nv08c_sensor.configure(NV08C_CONFIG_STARTUP, 1); 
  
  //Get pointer to output buffer 
  binr_chars = (char *) nv08c_sensor.value(NV08C_VALUE_OUTPUT);
  
  //Start-up is asynchronous, so we need to wait 
  WAIT(&et, 5*CLOCK_SECOND);

  //Ask GPS sensor to give Time data
  nv08c_sensor.configure(NV08C_CONFIG_SEND_CMD, (int) &time_command[APP_CMD_ON_TIME]);
  
  //Set up raw data bcaster
  abc_open(&gc, GPS_CHANNEL, &gc_callbacks);
  
  //Set up file handles 
  raw_file = cfs_open("raw.dat",CFS_APPEND); 
  sync_file = cfs_open("sync.dat", CFS_APPEND);
  
  if (raw_file == -1 || sync_file == -1) return; 
  while (t < DURATION*POLLING_HZ) {  
    WAIT(&et, CLOCK_SECOND/POLLING_HZ); 
    t++; 
    
    //Get a single BINR message from GPS sensor 
    //May be multiple iterations to fit all in.

    while ((length = nv08c_sensor.value(NV08C_VALUE_GET_CMD))) {
      cs = clock_seconds();
      ct = clock_time(); 
      time = message_is_time(binr_chars);
      cfs_write(raw_file, binr_chars,length);
      if (time) {
        cfs_write(sync_file, binr_chars, length);
        cfs_write(sync_file, &cs, sizeof(unsigned long));
        cfs_write(sync_file, &ct, sizeof(clock_time_t));
        FLASH_LED(LEDS_YELLOW);
      }
    }
  } 

  //Send send shutdown commands 
  nv08c_sensor.configure(NV08C_CONFIG_SHUTDOWN,1);
  cfs_close(raw_file);
  cfs_close(sync_file); 
  PROCESS_END();
}

static char mantra[] = {
 0x92, 0xc1, 0x53, 0xd2, 0x88, 0x57, 0x6e, 0xaf, 0x79, 0x4f, 0xc4, 0xf2, 0xcb, 0xe6, 0x84, 0x22,
 0xcc, 0x33, 0xe3, 0xf2, 0x41, 0x24, 0x48, 0xe2, 0xaa, 0xd2, 0x0b, 0x6c, 0xbe, 0x58, 0x5f, 0x5f,
 0xec, 0xa2, 0xb3, 0x41, 0xc5, 0x4d, 0xaa, 0x94, 0x7a, 0xeb, 0x88, 0x4e, 0xea, 0xd2, 0x91, 0xe4,
 0x5d, 0xe9, 0xbf, 0xea, 0xc7, 0x78, 0x54, 0x6f, 0xfb, 0x8f, 0x9c, 0x89, 0xa1, 0x1a, 0xbc, 0x6d,
 0xd4, 0x25, 0x3d, 0x51, 0xe1, 0x13, 0xb8, 0xfa, 0x86, 0x30, 0x71, 0x47, 0x81, 0x83, 0x4b, 0x14,
 0xbd, 0xb4, 0x37, 0x74, 0x5a, 0x56, 0x55, 0x7d, 0x52, 0x36, 0x2b, 0x94, 0x7c, 0x5d, 0x2d, 0x06,
 0x5e, 0x0f, 0x90, 0xa2, 0x12, 0xc9, 0x9f, 0xc4, 0x9e, 0x4c, 0x05, 0xe8, 0x67, 0x62, 0x6e, 0x22,
 0x01, 0x4a, 0x4f, 0xe2, 0x2b, 0x89, 0x84, 0x5d, 0x68, 0x33, 0x9f, 0xb9, 0x18, 0x12, 0xcc, 0xeb
};
static int log_file = -1;
int popcount (char a, char b) {
  char d = a^b;
  int c = 0; 
  while (d &= d-1) c++;
  return c;
}

void
bc_packet_received(struct abc_conn *bc) {
  
  int i, l, pc = 0;
  char log_buffer[48];
  for (i=0; i < packetbuf_datalen(); i++) {
    pc += popcount(*((char *) packetbuf_dataptr() + i), mantra[i]); 
  }
  l = sprintf(log_buffer,"%d,%d,%d,%d,%d\r\n", clock_seconds(), clock_time(),
      (int)packetbuf_attr(PACKETBUF_ATTR_LINK_QUALITY),
      i*8*sizeof(char), pc);
  if (log_file != -1 && l > 0 && cfs_write(log_file,log_buffer,l) > 0) {
      FLASH_LED(LEDS_GREEN);
  }

} 

void
bc_packet_sent(struct abc_conn *bc, int status, int num_tx) {
  FLASH_LED(LEDS_GREEN);
} 

static struct abc_callbacks bc_callbacks = {
 bc_packet_received, bc_packet_sent 
};
static struct abc_conn bc; 

PROCESS_THREAD(bee_proc, ev, data) {
  static struct etimer et; 
  PROCESS_EXITHANDLER(abc_close(&bc);); 
  PROCESS_BEGIN();
  abc_open(&bc, BEE_CHANNEL, &bc_callbacks); 
  log_file = cfs_open("ber.csv", CFS_APPEND);
  
  while (1) { 
#if 1
    WAIT(&et, CLOCK_SECOND*30);
#else 
    WAIT(&et, CLOCK_SECOND/4);    
    packetbuf_copyfrom(mantra, (random_rand() % MAX_PACKETLEN));
    abc_send(&bc);
#endif
  } 
  cfs_close(log_file); 
  PROCESS_END();
}
