#include "nv08c.h"
#include "dev/leds.h"
#include "uart.h"
#include "gpio.h"
#include "lib/sensors.h"
#include "lib/crc16.h"
#include <stdio.h>
#include <string.h>

static struct nv08c_connection nc;

static struct nv08c_command cmd_bible[] = {
#define NV08C_CMD_FREQ 0
  { 3, { 0xd7, 0x02, 0x02 } },

#define NV08C_CMD_SMOOTHING_INT 1
  { 4, { 0xd7, 0x03, 0x01, 0x00 } },

#define NV08C_CMD_GRP_DELAY 2
  { 3, { 0xa0, 0x03, 0x01 } }, 

#define NV08C_CMD_SATBITS 3
  { 2, { 0xd5, 0x01 } },

#define NV08C_CMD_GETRAW 4
  { 2, { 0xf4, 0x05 } },

#define NV08C_CMD_STOPTX 5
  { 1, { 0x0e } },
}; 
static struct nv08c_command msg_bible[] = {
#define NV08C_MSG_NULL 0
  { 2, { 0xe5, 0x00 } },
};
int
command_compare(char *i, int len, struct nv08c_command * tome, int cmd_index) {

}
int
send_binr(const char *s, int len) {
  int i = 0;
  uint16_t acc = 0;
  uart2_putc(NV08C_BINR_DLE); 
  //printf("10 "); 
  for(i = 0; i < len; i++) {
    if (s[i] == 0x10) {
      uart2_putc(s[i]); 
      //printf("%02X ",s[i]); 
      if (nc.crc_on) acc = crc16_add(s[i], acc);
    }
    uart2_putc(s[i]); 
    //printf("%02X ",s[i]); 
    if (nc.crc_on) acc = crc16_add(s[i], acc);
  }
  if (nc.crc_on) { 
    uart2_putc(NV08C_BINR_DLE); 
    uart2_putc(NV08C_BINR_CRC); 
    //printf("10 FF "); 
    uart2_putc((char) (acc >> 8));
    uart2_putc((char) (acc &= 0x00FF));
  } 
  uart2_putc(NV08C_BINR_DLE); 
  uart2_putc(NV08C_BINR_ETX); 
  //printf("10 03\r\n"); 
}

int
recv_raw(char * buf) {
  int read_length = 0; 
  if (nc.output_busy) return 0; 
  while (uart2_can_get() && read_length < NV08C_BINR_MAXLEN) {
    buf[read_length] = uart2_getc();
    if (nc.uart) uart1_putc(buf[read_length]);
    read_length++; 
  }
  return read_length; 
}

int
recv_binr(char *buf) {
  static int prev_is_dle = 0;
  int read_length = 0; 
  if (!nc.output_busy && !uart2_can_get()) return 0; 
  nc.output_busy = 1; 
  while (read_length < NV08C_BINR_MAXLEN) {
    buf[read_length] = uart2_getc();
    if (nc.uart) uart1_putc(buf[read_length]);
    if (prev_is_dle && buf[read_length] == NV08C_BINR_ETX) {
      nc.output_busy = 0;
      return read_length + 1; 
    } 
    prev_is_dle = (buf[read_length] == NV08C_BINR_DLE) ? !prev_is_dle : 0; 
    read_length++; 
  }
  return read_length; 

}

void
send_binr_from_bible(int offset) {
  send_binr(cmd_bible[offset].command,
      cmd_bible[offset].length);
}
void run_startup_script(void *vint);

void
run_startup_script(void *vint) {
  static int state = 0; 
  switch (state) { 
    case 0:
      send_binr_from_bible(NV08C_CMD_FREQ); 
      ctimer_set(&nc.startup_timer, CLOCK_SECOND/8, run_startup_script,0);
      state++;
      break;
    case 1:
      send_binr_from_bible(NV08C_CMD_SMOOTHING_INT);
      ctimer_set(&nc.startup_timer, 2*CLOCK_SECOND, run_startup_script,0);
      state++;
      break;
    case 2:
      send_binr_from_bible(NV08C_CMD_GRP_DELAY);
      ctimer_set(&nc.startup_timer, CLOCK_SECOND/8, run_startup_script,0);
      state++;
      break;
    case 3:
      send_binr_from_bible(NV08C_CMD_SATBITS);
      ctimer_set(&nc.startup_timer, CLOCK_SECOND/8, run_startup_script,0);
      state++;
      break;
    case 4:
      send_binr_from_bible(NV08C_CMD_GETRAW);
      state = 0;
      break;
  }
}
int
configure_uart(void) {
  /* This line enables one stop bit odd parity */ 
  *UART2_UCON |= 0xC; 
  printf("UART2_UCON: %X\n",*UART2_UCON); 
}
void
gps_reset_high(void * e) {
  gpio_set(GPIO_08);
  printf("GPS Reset\n");
}

int
gps_reset(void) {
  GPIO->PAD_DIR_SET.GPIO_08 = 1;
  GPIO->FUNC_SEL.GPIO_08 = 0;
  gpio_reset(GPIO_08);
  ctimer_set(&nc.startup_timer, CLOCK_SECOND/4, gps_reset_high, NULL);
}
int
debug_msg(void) {
  char msg[] = "Hello World!\r\n";
  int i = 0;
  while (msg[i]) uart2_putc(msg[i++]);
}
  static int
configure(int type, int c)
{
  switch (type) {
    case SENSORS_ACTIVE:
      memset(&nc, 0, sizeof(struct nv08c_connection)); 
      uart2_init(INC, MOD, SAMP); 
      configure_uart(); 
      break;
    case NV08C_CONFIG_PIPE_TO_SERIAL:
      nc.uart = c; 
      break; 
    case NV08C_CONFIG_CRC:
      nc.crc_on = c; 
      break; 
    case NV08C_CONFIG_STARTUP:
      run_startup_script(0);      
      break; 
    case NV08C_CONFIG_INPUT:
      nc.input = (char *) c; 
      break; 
    case NV08C_CONFIG_SHUTDOWN:
      send_binr_from_bible(NV08C_CMD_STOPTX);
      break; 
    case NV08C_CONFIG_SEND_CMD:
  send_binr(((struct nv08c_command *) c)->command,
      ((struct nv08c_command *) c)->length);
      break; 
  }
  return 1;
}

  static int
value(int type)
{
  switch (type) {
    case NV08C_VALUE_OUTPUT:
      return (int) nc.output;	
    case NV08C_VALUE_GET_CHUNK:
      return recv_raw(nc.output);
    case NV08C_VALUE_GET_CMD:
      return recv_binr(nc.output);
    case NV08C_VALUE_BUSY:
      return nc.output_busy;
  }
}

static int
status(int type) {
  return 0;
}

SENSORS_SENSOR(nv08c_sensor, "nv08c-sensor", value, configure, status);
