#include "contiki.h"
#include "net/rime.h"
#include "dev/leds.h"

#define WAIT(e,t)   etimer_set(e, t);\
        PROCESS_WAIT_EVENT_UNTIL(etimer_expired(e));

#define FLASH_LED(l) {leds_on(l); clock_delay_msec(50); leds_off(l); clock_delay_msec(50);}

#define GPS_CHANNEL 136

PROCESS(gps_proc, "GPS collection");
AUTOSTART_PROCESSES(&gps_proc);

void
gc_packet_received(struct abc_conn *gc) {
  int i;
  for (i=0; i < packetbuf_datalen(); i++) {
#if 1
    printf("%02x ",*((char *) packetbuf_dataptr() + i));
#else
    uart1_putc(*((char *) packetbuf_dataptr() + i)); 
#endif
  }
    printf("\r\n\r\n");
  
  FLASH_LED(LEDS_GREEN);
}

void
gc_packet_sent(struct abc_conn *gc, int status, int num_tx) {

}

static struct abc_callbacks gc_callbacks = { gc_packet_received, gc_packet_sent };
static struct abc_conn gc;

PROCESS_THREAD(gps_proc, ev, data)
{
  static struct etimer et;
  PROCESS_EXITHANDLER(abc_close(&gc);); 
  PROCESS_BEGIN();
  abc_open(&gc, GPS_CHANNEL, &gc_callbacks);
  while (1) { 
    WAIT(&et, CLOCK_SECOND*30);
  } 
  PROCESS_END();
}
