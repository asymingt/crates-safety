#ifndef _NV08C_H_
#define _NV08C_H_
#include "contiki.h"
#include "uart.h"
#define NV08C_VALUE_OUTPUT 0
#define NV08C_VALUE_GET_CHUNK 1
#define NV08C_VALUE_GET_CMD 2
#define NV08C_VALUE_OUTPUT_READY 3
#define NV08C_VALUE_READ_LENGTH 4
#define NV08C_VALUE_BUSY 5

#define NV08C_CONFIG_CRC 0
#define NV08C_CONFIG_STARTUP 1
#define NV08C_CONFIG_INPUT 2
#define NV08C_CONFIG_SEND_CMD 3
#define NV08C_CONFIG_SHUTDOWN 4
#define NV08C_CONFIG_PIPE_TO_SERIAL 5

#define NV08C_BINR_DLE 0x10
#define NV08C_BINR_ETX 0x03
#define NV08C_BINR_CRC 0xFF
#define NV08C_BINR_MAXLEN 512 

extern const struct sensors_sensor nv08c_sensor;
struct nv08c_command {
  int length;
  char command[NV08C_BINR_MAXLEN];
};

struct nv08c_connection {
  char output[NV08C_BINR_MAXLEN], *input;
  struct ctimer startup_timer;
  int crc_on, output_busy, read_length, uart;
};
#endif /* NV08C_H_ */
