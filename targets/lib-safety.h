#ifndef _SAFETY_H_
#define _SAFETY_H_

// Standard C includes
#include <stdio.h>
#include <string.h>
#include <stdint.h>

// For networking
#include "net/rime.h"

// BASIC GEOMETRY /////////////////////////////////////////////////////////////////

// 2D point
typedef struct _point {
	float x;
	float y;
}__attribute__((packed)) point;

// 2D point
typedef struct _triple {
	float x;
	float y;
	float z;
}__attribute__((packed)) triple;

 // Rectangle
typedef struct rectangle {
	point p[4];
}__attribute__((packed)) rectangle;

// Circle
typedef struct _circle {
	point  m;
	float  r;
}__attribute__((packed)) circle;

// A zone
typedef struct _zone {
	int    n;
	float* vertx;
	float* verty;
	float  altitude;
	float  voltage;
}__attribute__((packed)) zone;

// Check if (x,y) lies within the polygon define by [vertx1,verty1]...[vertxN,vertyN]
int is_point_in_polygon(int n, float *vertx, float *verty, point *p);

// Check to see if two rotated rectangles intersect
int do_rectangles_overlap(rectangle *rec0, rectangle *rec1);

// DEFINE ALL POSSIBLE STATES THAT A PLATFORM CAN BE IN ////////////////////////////

// Possible states
typedef enum
{
	UAV_STATE_INIT,	// Waiting for association / correct zone
	UAV_STATE_SAFE,	// Safe flying mode
	UAV_STATE_STOP,	// Stop immediately (to prevent collision)
	UAV_STATE_LAND,	// Land immediately (when outside comm / safe zone / altitude)
	UAV_STATE_KILL	// Kill immediately (when not responding)
}
UAV_STATE;

// COMMUNICATION PROTOCOL //////////////////////////////////////////////////////////

// Radio parameters
#define RADIO_CHANNEL      	129
#define MAX_PLATFORMS   	8

// Time inertvals
#define TIME_ASSOCIATION    CLOCK_SECOND
#define TIME_COMMAND        CLOCK_SECOND
#define TIME_HEARTBEAT      CLOCK_SECOND

// Timeouts
#define TIMEOUT_COMMAND     CLOCK_SECOND
#define TIMEOUT_HEARTBEAT   CLOCK_SECOND

// Packet descriptors
#define SAFETY_ID_ASSOC_REQ 0x1
#define SAFETY_ID_ASSOC_RES 0x2
#define SAFETY_ID_HEARTBEAT 0x3
#define SAFETY_ID_COMMAND   0x4

// Special addresses
extern rimeaddr_t rimeaddr_broadcast;
extern rimeaddr_t rimeaddr_ground;

// Packetization [ ADDRESS 8 ] [ PACKET DESCRIPTOR 1 ] [ PAYLOAD ] [ CRC 2 ]

typedef struct _SAFETY_HEADER {
    unsigned char addr[8];
    unsigned char id;
}__attribute__((packed)) SAFETY_HEADER;

// Every message ends with this
typedef struct _SAFETY_FOOTER {
    unsigned short crc;
}__attribute__((packed)) SAFETY_FOOTER;

// Association response
typedef struct _SAFETY_ASSOC_REQ {
	rimeaddr_t addr;
}__attribute__((packed)) SAFETY_ASSOC_REQ;

// Association request
typedef struct _SAFETY_ASSOC_RES {
	rimeaddr_t addr;								// New RIME address
}__attribute__((packed)) SAFETY_ASSOC_RES;

// Position and velocity of a UAV (collision prevention and status)
typedef struct _SAFETY_HEARTBEAT {
	unsigned char state;							// Instruction to send to UAV
	triple position;								// Position of the UAV
	triple velocity;								// Velocity of the UAV
	float voltage;									// Energy remaining
}__attribute__((packed)) SAFETY_HEARTBEAT;

// Command vector
typedef struct _SAFETY_COMMAND {
	unsigned short num;								// Total number of platforms in the system			
	rimeaddr_t address[MAX_PLATFORMS];				// Platform address			
	unsigned char command[MAX_PLATFORMS];			// Platform command
}__attribute__((packed)) SAFETY_COMMAND;

// External packets
extern SAFETY_ASSOC_REQ  safety_assoc_req;
extern SAFETY_ASSOC_RES  safety_assoc_res;
extern SAFETY_HEARTBEAT  safety_heartbeat;
extern SAFETY_COMMAND    safety_command;

// Parser state
typedef enum 
{  
   STATE_SAFETY_WAITA,
   STATE_SAFETY_WAITL1,
   STATE_SAFETY_WAITL2,
   STATE_SAFETY_WAITT,
   STATE_SAFETY_WAITD,
   STATE_SAFETY_WAITC1,
   STATE_SAFETY_WAITC2,
} 
safety_state;

// Encode a packet to  the wire
unsigned short safety_encode(void *buff, rimeaddr_t *dest, unsigned char id);

// Decode a packet from the wire
unsigned char safety_decode(void *buff, int n);

// USEFUL EXTRAS ///////////////////////////////////////////////////////////////////

// Print a floating point number [http://www.ragestorm.net/blogs/?p=57]
void printfloat(float a);


/*

// COMMAND AND HEARTBEAT PARAMETERS ////////////////////////////////////////////////

// Heartbeat parameters (state uses MD5 to avoid malformed / malicious packets)
#define COMMAND_TIME        CLOCK_SECOND
#define HEARTBEAT_TIME      CLOCK_SECOND
#define HEARTBEAT_TIMEOUT   CLOCK_SECOND*5

// Hardware information
#define FLIGHT_POLL_TIME    CLOCK_SECOND
#define FLIGHT_KILL_GPIO    1

// Add a 10ms delay to ease up
#define UART_WAIT_TIME      CLOCK_SECOND/100

// Maximum number of UAVs
#define MAX_NUM_PLATFORMS   8

// DEFINE THE BROADCAST CODES FOR THE STATUS COMMANDS //////////////////////////////

// Possible command codes
#define CMD_OK              "abcd"
#define CMD_HOVER        	"efgh"
#define CMD_LAND        	"ijkl"
#define CMD_KILL        	"nmop"
#define CMD_LENGTH          4

// NETWORK PROTOCOL ////////////////////////////////////////////////////////////////
//
// All packets share this structure : [UINT8 ADD] [UINT8 TAG] ....
// Where ADD is the receiver address
//       TAG is the message time

// The radio channel over which safety-critical information is announced
#define RADIO_CHANNEL      	129

// RIME addresses for nodes
#define ADD_UAV_BASE     	(uint8_t) 0x00
#define ADD_BS				(uint8_t) 0xFE
#define ADD_BROADCAST		(uint8_t) 0xFF

// The identifier tags
#define TAG_ASSOCIATION_REQ (uint8_t) 0x00
#define TAG_ASSOCIATION_RES (uint8_t) 0x01
#define TAG_HEARTBEAT 		(uint8_t) 0x02
#define TAG_COMMAND 		(uint8_t) 0x03
#define TAG_GNSS_RAW     	(uint8_t) 0x04
#define TAG_GNSS_COR        (uint8_t) 0x05

// The maximum length of data
#define MAX_DATA_LENGTH     120

// Data packet offsets
#define OFFSET_ADDRESS 		0
#define OFFSET_TAG 			1
#define OFFSET_DATA 		2

// PACKET HEADER
typedef struct _header {
	const uint8_t add;								// Address
	const uint8_t tag;								// Tag (content type)
}__attribute__((packed)) header;

// BS -> UAVs : Commands
typedef struct _pkt_command {
	header hdr;										// Standard header
	uint8_t add[MAX_NUM_PLATFORMS];					// State of each UAV
	uint8_t cmd[MAX_NUM_PLATFORMS][CMD_LENGTH];		// Commands for each UAV
}__attribute__((packed)) pkt_command;

// UAV -> BS : association request
typedef struct _pkt_association_req {
	header hdr;										// Standard header
}__attribute__((packed)) 
pkt_association_req;

// BS -> UAV : association response
typedef struct _pkt_association_res {
	header hdr;										// Standard header
	uint8_t add;									// New address
}__attribute__((packed)) 
pkt_association_res;

// UAV -> BS : GNSS raw measurements
typedef struct _pkt_gnss_raw {
	header hdr;										// Standard header
	uint8_t data[MAX_DATA_LENGTH];					// Data content
}__attribute__((packed)) 
pkt_gnss_raw;

// BS -> UAV : GNSS corrections
typedef struct _pkt_gnss_cor {
	header hdr;										// Standard header
	uint8_t data[MAX_DATA_LENGTH];					// Data content
}__attribute__((packed)) 
pkt_gnss_cor;

// AscTec low-level data
typedef struct _pkt_asctec_llp {
	header hdr;
}__attribute__((packed)) 
pkt_asctec_llp;

*/


#endif