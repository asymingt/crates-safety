/////////////////////////////////////////////////////////////////////////////////////////
// VISUAL PROCESS ///////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
// This process manages the LEDs. In the code examples for the Orisen Stamp, a blocking
// delay was used to flash the LEDs. This code uses the contiki etimers, to free up 
// resources for other safety module features -- UART streaming, heartbeats, etc.
/////////////////////////////////////////////////////////////////////////////////////////

#ifndef _PROCESS_LEDS_H_
#define _PROCESS_LEDS_H_

#include "contiki.h"
#include "dev/leds.h"

// Basic LED definitions
#define LEDS_BLINK_NORM        CLOCK_SECOND/20
#define LEDS_BLINK_FAST        CLOCK_SECOND/10
#define LEDS_FLASH_NORM        CLOCK_SECOND/2
#define LEDS_FLASH_FAST        CLOCK_SECOND/1
#define LEDS_G 			       (1 << 0)
#define LEDS_Y                 (1 << 1)
#define LEDS_B                 (1 << 2)
#define LEDS_R                 (1 << 2)
#define LEDS_ON         	   (1 << 3)
#define LEDS_OFF        	   (1 << 4)
#define LEDS_FLASH       	   (1 << 5)
#define LEDS_BLINK       	   (1 << 6)
#define LEDS_FAST       	   (1 << 7)
#define LEDS_COUNT             3

// An event that signals the arrival of a GPS string
extern process_event_t event_leds;

// Define the contiki process for managing the LEDs
PROCESS_NAME(process_leds);

#endif